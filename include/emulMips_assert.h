#ifndef EMULMIPS_ASSERT_H_
#define EMULMIPS_ASSERT_H_

#include "emulMips.h"

int assertCmd(interpreteur inter, cpu_s cpu);
int assertCmdWord(interpreteur inter, cpu_s cpu);
int assertCmdWordReg(registreTable_s regT, uint32_t val, char* chaine);
int assertCmdWordRAM(RAM_s mem, uint32_t val, uint32_t address);
int assertCmdByte(interpreteur inter, RAM_s memmap);

#endif
