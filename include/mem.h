#ifndef MEM_H_
#define MEM_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

/* macros de DEBUG_MSG fournies , etc */
#include "common/notify.h"

#define nb_seg 5


// REGISTRE //
typedef struct {
  char * name;
  int32_t val;
} *registre_s, registreTable_s[35];

void set_reg_name(registre_s reg, char * name);
void set_reg_val(registre_s reg, int32_t val);
char * get_reg_name(registre_s reg);
int32_t get_reg_val(registre_s reg);
registre_s get_reg(registreTable_s regTab, int numero);
void init_reg(registreTable_s reg);
void free_reg_name(registre_s reg);
registre_s reg_locate(registreTable_s reg, char* chaine);

// SEGMENT //
typedef struct segment {
  char * name;
  int taille; // Taille reelle du segment en octet
  char * droit; // Droit d'écriture/de lecture ...
  uint32_t adresse; // Adresse virtuelle du début du bloc sur 32bits
  uint8_t* data; //  Le tableau des mots de 32 bits
} *segment_s, RAM_s[nb_seg]; // Tableau de segments

void set_seg_name(segment_s seg, char * name);
void set_seg_taille(segment_s seg, int taille);
void set_seg_droit(segment_s seg, char * droit);
void set_seg_adresse(segment_s seg, uint32_t adresse);
void set_seg_data(segment_s seg, uint8_t * data);
char * get_seg_name(segment_s seg);
int get_seg_taille(segment_s seg);
char * get_seg_droit(segment_s seg);
uint32_t get_seg_adresse(segment_s seg);
uint8_t * get_seg_data(segment_s seg);
segment_s seg_locate(RAM_s mem, uint32_t add);
uint32_t get_word(RAM_s mem, uint32_t startAdd);
void set_word(RAM_s mem, uint32_t val, uint32_t startAdd);

void alloc_1segment_memoire(segment_s seg, int taille);
void free_1segment_memoire(segment_s seg);
void free_RAM_s(RAM_s r, int nseg);

// CPU //
typedef struct {
  registreTable_s reg; //35 registres du CPU de 32 bits chacun
  RAM_s mem; //mémoire en octet
  int nseg; // Nombre de segments utilisé pour le cpu
} * cpu_s;

void set_cpu_reg(cpu_s cpu, int num, char *name);
void set_cpu_nseg(cpu_s cpu, int nseg);
int get_cpu_nseg(cpu_s cpu);
segment_s get_cpu_mem(cpu_s cpu);
registre_s get_cpu_reg(cpu_s cpu);

void init_all_segment(cpu_s cpu, int nseg);
cpu_s init_cpu(int nseg );
void free_cpu(cpu_s cpu);

#endif
