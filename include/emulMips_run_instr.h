#ifndef INSTR_H_
#define INSTR_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* la librairie readline */
#include <readline/readline.h>
#include <readline/history.h>
#include "mem.h"
/* macros de DEBUG_MSG fournies , etc */
#include "common/notify.h"

#include "emulMips.h"
#include "mem.h"
#include "emulMips_disasm_code.h"

int toutvabien(instruction_s instr, cpu_s cpu, instruction_s nInstr);
void nextPC(cpu_s cpu);

int instr_AND(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_ANDI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BEQ(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BGEZ(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BGTZ(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BNE(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BLEZ(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BLTZ(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_J(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_JAL(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_JALR(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_JR(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_LB(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_LW(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_LUI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_LBU(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SB(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SEB(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SW(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_MFLO(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_MFHI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_OR(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_ORI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_XOR(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_NOP(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SLL(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SRL(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SRA(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SLT(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SLTI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SLTU(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SLTUI(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_SYSCALL(instruction_s instr, cpu_s cpu, instruction_s nInstr);
int instr_BREAK(instruction_s instr, cpu_s cpu, instruction_s nInstr);

///////////////////////////////////
// Les fonctions d'overflow
int overflow_add_32(int32_t a, int32_t b);
int overflow_sub_32(int32_t a, int32_t b);
int overflow_mult_32(int32_t a, int32_t b);

///////////////////////////////////
// Les instructions arithmetiques
int instr_ADD(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_ADDI(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_ADDIU(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_ADDU(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_SUB(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_SUBU(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_MULT(instruction_s inst, cpu_s cpu, instruction_s nInstr);
int instr_DIV(instruction_s inst, cpu_s cpu, instruction_s nInstr);
///////////////////////////////////

#endif
