#ifndef EMULMIPS_H_
#define EMULMIPS_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* la librairie readline */
#include <readline/readline.h>
#include <readline/history.h>
#include "mem.h"
#include "emulMips_disasm_dico.h"
/* macros de DEBUG_MSG fournies , etc */
#include "common/notify.h"

extern char* nom_registre[];
extern char* nom_segment[];
extern uint32_t Vaddr_segment[];
extern int taille_virt_segment;
extern int taille_re_segment;


/* prompt du mode shell interactif */
#define PROMPT_STRING "MipsShell : > "

/* taille max pour nos chaines de char */
#define MAX_STR 1024


/*************************************************************\
Valeur de retour speciales pour la fonction
	int execute_cmd(interpreteur inter) ;
Toute autre valeur signifie qu'une erreur est survenue
 \*************************************************************/
#define CMD_INVALID_ARGS_RETURN_VALUE 2
#define CMD_NO_ARGS_RETURN_VALUE 1
#define CMD_OK_RETURN_VALUE 0
#define CMD_EXIT_RETURN_VALUE -1
#define CMD_UNKOWN_RETURN_VALUE -2


/* type de token (exemple) */
enum {HEXA,REG,UNKNOWN};

/* mode d'interaction avec l'interpreteur (exemple)*/
typedef enum {INTERACTIF,SCRIPT,DEBUG_MODE} inter_mode;

/* structure passée en parametre qui contient la connaissance de l'état de
 * l'interpréteur
 */
typedef struct {
    inter_mode mode;
    char input[MAX_STR];
    FILE* fp;
    char * from;
    char first_token;
} *interpreteur;

/**
 *  Les setters et les getters de la structure interpreteur
 *                                 - from -
 *  Il n'y a pas de setter ni de getter pour inter->from pour la simple raison
 *  que son utilisation est très spécial. Il sert de référence pour faire la
 *  lecture de la ligne. Donc un set est d'aucune utilité premièrement.
 *  Secondement le getter renvoie la valeur oui, mais nous voulons l'addresse
 *  mais le problème c'est que on peut pas faire l'operand '&' sur le getter
 *  directement. Donc en fait il faudrait le mettre dans une autre variable.
 *  Il faudrait initialiser cette variable en même temps que first_token passe
 *  à 0. Mais on ne peut pas car il y a un appel à strtok_r dans le else.
 *  Il faudrait le définir en dehors mais le définir toujours tant que
 *  first_token ne soit pas à 0 ... Bref c'est très galère, j'ai essayé des trucs
 *  mais rien de très simples s'en ai dégager ... Au vu de l'utilisation
 *  spéciale de ce membre de structure, on se permet de ne pas faire de getter
 *  pour lui.
 */

void set_interpreteur_mode(interpreteur inter, inter_mode mode);
void set_interpreteur_fp(interpreteur inter, FILE* fp);
void set_interpreteur_input(interpreteur inter, char* input);
void set_interpreteur_first_token(interpreteur inter, char first_token);

inter_mode get_interpreteur_mode(interpreteur inter);
FILE* get_interpreteur_fp(interpreteur inter);
char* get_interpreteur_input(interpreteur inter);
char get_interpreteur_first_token(interpreteur inter);

/**
 * allocation et init interpreteur
 * @return un pointeur vers une structure allouée dynamiquement
 */
interpreteur init_inter(void);

/**
 * desallocation de l'interpreteur
 * @param inter le pointeur vers l'interpreteur à libérer
 */
void del_inter(interpreteur inter);

/**
 * return le prochain token de la chaine actuellement
 * analysée par l'interpreteur
 * La fonction utilise une variable interne de l'interpreteur
 * pour gérer les premiers appels a strtok
 * @inter le pointeur vers l'interpreteur
 * @return un pointeur vers le token ou NULL
 */
char* get_next_token(interpreteur inter);

/**
 * teste si un token est une valeur hexa
 * ATTENTION cette méthode n'est pas complete et ne fonctionnera pas dans tous les cas
 * essayer avec 0x567ZRT...
 *@param chaine le token à analyser
 *@return 0 si non-hexa, non null autrement
 */
int is_hexa(char* chaine);

/**
 * Renvoie 1 si chaine ne possède que des chiffres
 * controle que la valeur soit sur 8 bits au max
 */
 int is_readable(segment_s seg);
 int is_writable(segment_s seg);
 int is_executable(segment_s seg);


int is_number8(char* chaine);

/**
 * Renvoie 1 si chaine ne possède que des chiffres
 * controle que la valeur soit sur 32 bits au max
 */
int is_number32(char* chaine);

/**
* On vérifie que c'est bien une adresse hexa n bits
* retourne 1 si c'est le cas
* Cette fonction est compliqué un peu. Car sscanf, atoi, strtol, ne permettent
* de détecter les overflow, ils vont juste tronquer, nous on veut détecter !
*/

int is_hexa_format(char* chaine, int n);

int is_reg(char* chaine);

/**
 * retourne le type du token (fonction très incomplete)
 * @param chaine le token à analyser
 * @return un entier correspondant au type du token
 *
 * Il faudra ajouter les autres token, il faudrait faire la liste de tout les
 * tokens, avec une fonction "is_typeToken" pour vérifier la validité du token.
 */
int get_type(char* chaine);

/*************************************************************\
 Les deux fonctions principales de l'émulateur.
	execute_cmd: parse la commande et l'execute en appelant la bonne fonction C
	acquire_line : recupere une ligne (donc une "commande") dans le flux
 \*************************************************************/


/**
*
* @brief parse la chaine courante de l'interpreteur à la recherche d'une commande, et execute cette commande.
* @param inter l'interpreteur qui demande l'analyse
* @return CMD_OK_RETURN_VALUE si la commande s'est exécutée avec succès (0)
* @return CMD_EXIT_RETURN_VALUE si c'est la commande exit. Dans ce cas, le programme doit se terminer. (-1)
* @return CMD_UNKOWN_RETURN_VALUE si la commande n'est pas reconnue. (-2)
* @return tout autre nombre (eg tout nombre positif) si erreur d'execution de la commande
*/
int execute_cmd(interpreteur inter, cpu_s cpuMips, dico_s d);

/**
 * @param in Input line (possibly very badly written).
 * @param out Line in a suitable form for further analysis.
 * @return nothing
 * @brief This function will prepare a line for further analysis.
 *
 * This function will prepare a line for further analysis and check for low-level syntax errors.
 * colon, brackets, commas are surrounded with blank; tabs are replaced with blanks.
 * negs '-' are attached to the following token (e.g., "toto -   56" -> "toto -56")  .
 */
void string_standardise( char* in, char* out );

/**
 * Résumé : Quelque soit le mode d'entrée (script ou taper) on fout la ligne de
 * de code dans la variable inter->input. La chaine est formaté.
 * @brief extrait la prochaine ligne du flux fp.
 * Si fp ==stdin, utilise la librairie readline pour gestion d'historique de commande.
 *
 * @return 0 si succes.
 * @return un nombre non nul si aucune ligne lue
 */
int  acquire_line(FILE *fp, interpreteur inter);

void usage_ERROR_MSG( char *command );

#endif
