#ifndef DICO_H_
#define DICO_H_

#include <stdint.h>
#include "mem.h"
#include "emulMips_disasm_code.h"

#define NB_INSTR 43

typedef struct _dico_s{
    code_s codeV;
    uint32_t masque;
    char type;
    char *nom;
    char* *op;
    int nbOp;
    void (*affichage)(instruction_s instr);
    int (*instruction)(instruction_s instr, cpu_s cpu,instruction_s nInstr);
} *dico_s;

dico_s build_dico(char* txtFile);
void moche(dico_s dico);
void dico_del(dico_s d);

code_s get_dico_code(dico_s d);
uint32_t get_dico_masque(dico_s d);
char get_dico_type(dico_s d);
char* get_dico_nom(dico_s d);
char** get_dico_op(dico_s d);
int get_dico_nbOp(dico_s d);

instruction_s instru_from_dico(code_s word, dico_s d);
instruction_s get_instru(code_s word, dico_s d);

#endif
