#ifndef RUN_H_
#define RUN_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* la librairie readline */
#include <readline/readline.h>
#include <readline/history.h>
/* macros de DEBUG_MSG fournies , etc */
#include "common/notify.h"

#include "emulMips.h"
#include "mem.h"
#include "emulMips_disasm_dico.h"

int runCmd(interpreteur inter, cpu_s cpu, dico_s dico);
int runCmd_instr(cpu_s cpu, dico_s dico);

#endif
