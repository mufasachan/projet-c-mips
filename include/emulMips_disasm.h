#ifndef DISASM_H_
#define DISASM_H_

#include "emulMips.h"
#include "mem.h"
#include "emulMips_disasm_dico.h"
#include "emulMips_disasm_code.h"

int disasmCmd(interpreteur inter, cpu_s cpu, dico_s d);

void disasmCmd_affichage(instruction_s instr, uint32_t add1);

#endif
