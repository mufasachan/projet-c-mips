#ifndef EMULMIPS_SET_H_
#define EMULMIPS_SET_H_

#include "emulMips.h"

int setCmd(interpreteur inter, cpu_s cpu);
int setCmdRAM(interpreteur inter, RAM_s memmap);
int setCmdRAMType(interpreteur inter, RAM_s memmap,char* chaine);
int setCmdRAMTypeByte(interpreteur inter, RAM_s memmap,char* chaine);
int setCmdRAMTypeWord(interpreteur inter, RAM_s memmap,char* chaine);
int setCmdReg(interpreteur inter, registreTable_s regT);
int setCmdRegVal(interpreteur inter, registreTable_s regT, char* chaine);

#endif
