#ifndef EMULMIPS_EXIT_H_
#define EMULMIPS_EXIT_H_

#include "emulMips.h"

/**
 * commande exit qui ne necessite pas d'analyse syntaxique
 * @param inter l'interpreteur qui demande l'analyse
 * @return 0 en case de succes, un nombre positif sinon
 */
int exitcmd(interpreteur inter);

#endif
