#ifndef CODE_H_
#define CODE_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "mem.h"

extern char* nom_registre[];
extern char* nom_segment[];
extern uint32_t Vaddr_segment[];
extern int taille_virt_segment;
extern int taille_re_segment;


typedef struct
{
  uint32_t fonc:6, sa:5, rd:5, rt:5, rs:5, opcode:6;
} R_s;

typedef struct
{
  uint32_t imm:16, rt:5, rs:5,  opcode:6;
} I_s;

typedef struct
{
  uint32_t target:26, opcode:6;
} J_s;

typedef union {
  R_s r;
  I_s i;
  J_s j;
  uint32_t code;
} *code_s;

code_s build_code(uint32_t c);
void code_del(code_s c);

uint32_t get_code(code_s codeV);
void set_code(code_s codeV,uint32_t c);

void code_swap(code_s c);

typedef struct _instruction_s{
  code_s code;
  char type;
  char *nom;
  char* *op;
  int nbOp;
  void (*affichage)(struct _instruction_s* instr);
  int (*instruction)(struct _instruction_s* instr, cpu_s cpu, struct _instruction_s* nInstr);
} *instruction_s;

code_s get_instr_code(instruction_s i);
char get_instr_type(instruction_s i);
char* get_instr_nom(instruction_s i);
char** get_instr_op(instruction_s i);
int get_instr_nbOp(instruction_s i);

void instr_affichage(instruction_s instr);
//  Pointeurs Affichage
void instr_affichage_LB(instruction_s instr);
void instr_affichage_classique(instruction_s instr);
//  Pointeurs Execution

#endif
