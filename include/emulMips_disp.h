#ifndef EMULMIPS_DISP_H_
#define EMULMIPS_DISP_H_

#include "emulMips.h"

int dispCmd(interpreteur inter, cpu_s cpuMips);
int dispCmdReg(interpreteur inter, registreTable_s regT);
int dispCmdRegOne(registreTable_s regT, char* chaine, int* compteur_ptr);
int dispCmdRegAll(registreTable_s regT);
int dispCmdRAM(interpreteur inter, RAM_s memmap);
int dispCmdRAMMap(interpreteur inter, RAM_s memmap);
int dispCmdRAMPlage(interpreteur inter, RAM_s memmap, char* chaine);

#endif
