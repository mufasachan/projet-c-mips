#ifndef EMULMIPS_LOAD_H_
#define EMULMIPS_LOAD_H_

#include "emulMips.h"

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include "common/bits.h"
#include "common/notify.h"
#include "elf/elf.h"
#include "elf/syms.h"
#include "elf/relocator.h"
#include "mem.h"
#include "mem_elfapi.h"

int is_in_symbols(char* name, stab symtab);
unsigned int get_nsegments(stab symtab,char* section_names[],int nb_sections);
int elf_load_section_in_memory(FILE* fp, mem memory, char* scn,unsigned int permissions,unsigned long long add_start);
void reloc_segment(FILE* fp, segment seg, mem memory,unsigned int endianness,stab symtab);
void print_segment_raw_content(segment* seg);
void edit_seg(cpu_s cpu, mem memory);
int check_add(uint32_t segment_start);
int check_start_add(uint32_t segment_start);
int check_mem_add(mem memory);
int loadCmd(interpreteur inter, cpu_s cpuMips);
int loadCmd_elf(interpreteur inter, cpu_s cpuMips, FILE * pf_elf, char * token, char * argv[]);
int loadCmd_elf_adr(interpreteur inter, cpu_s cpuMips, FILE * pf_elf, unsigned int next_segment_start, char * argv[]);

#endif
