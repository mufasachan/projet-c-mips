README DU PROJET INFO - EMULATEUR MIPS

########################################
# Creation Septembre 2019
########################################
# No restriction on usage nor dissemination
########################################
#Update : 9/10/2019 (version LIVRABLE_2)
########################################
# Problèmes, commentaires : mathias.montginoux@grenoble-inp.org ou mathias.reus@grenoble-inp.org
########################################


###################################################
############# Information générale ################

"
L'interpréteur utilise libreadline. Sur Linux, il faut donc installer le package de développement
 qui convient. Il apparaît que l'appel à lreadline n'est pas toujours suffisant pour compiler et
 nécessite parfois d'ajouter -lcurses selon les versions et l'état de votre OS. Ne pas hésiter à
 chercher sur le net ou nous consulter si vous avez des problèmes d'installation.  "
  (cf. README.txt donné au début du projet)

les sources (fichier .c) se trouvent dans
./src/

les includes (fichier .h) dans
./include/

Les fichiers de script de test se trouvent dans
./test/

###################################################
###### Compilation et execution du programme ######

Pour lancer ./emulMips em mode debug
1) $ make debug
2) $ ./emulMips

Pour lancer ./emulMips sans le mode debug
1) $ make release
2) $ ./emulMips

Pour executer avec un script
$ ./emulMips [nom_du_script.txt]

Pour nettoyer
$ make clean

pour créer une archive
$ make archive

##################################################
###################### Test ######################

Pour lancer les jeux de tests de la commande "disp":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/testcmdDisp/*.emu

Pour lancer les jeux de tests de la commande "set":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/testcmdSet/*.emu

Pour lancer les jeux de tests de la commande "assert":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/assert/*.emu

Pour lancer les jeux de tests de la commande "resume":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/resume/*.emu

Pour lancer les jeux de tests de la commande "debug":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/debug/*.emu

Pour lancer les jeux de tests de la commande "load":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/load/*.emu

Pour lancer les jeux de tests de la commande "disasm":
entrer la commande ./simpleUnitTest.sh -e ./emulMips test/disasm/*.emu

  ##### A noter #####
        Pour tester les commandes 'resume' et 'debug' il existe aussi des scripts de test.

      On lance ces scripts avec la commande suivante :
./emulMips <nom_du_script.txt>

      puis il faut intéragir avec l'interpreteur lorsque c'est possible, et relancer
      la lecture du script avec la commande 'resume' .
      parmi les scripts : script_test.txt ; script1.txt ; script2.txt
      (exemple : ./emulMips script_test.txt)

        Pour tester les commandes 'load' et 'disasm', il existe des fichiers ELF
      (boucle.o et pile.o) qu'on lance avec les commandes :
      $ ./emulMips
      MipsShell : > load <ELF_file.o> addresse
      MipsShell : > disasm <plage>+

      car les jeux de tests ne fonctionnent pas tous (un segfault avec les tests.emu
      et pas avec les commandes 'directs' ci-dessus).

##################################################
#################### END #########################
