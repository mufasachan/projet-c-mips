#include "emulMips.h"
#include "emulMips_assert.h"
#include "emulMips_disp.h"
#include "emulMips_exit.h"
#include "emulMips_set.h"
#include "emulMips_resume.h"
#include "emulMips_debug.h"
#include "emulMips_load.h"
#include "emulMips_disasm.h"
#include "emulMips_run.h"


void set_interpreteur_mode(interpreteur inter, inter_mode mode)
{
  inter->mode = mode;
}

void set_interpreteur_fp(interpreteur inter, FILE* fp)
{
  inter->fp = fp;
}

void set_interpreteur_input(interpreteur inter, char* input)
{
  strncpy(inter->input,input,MAX_STR);
}

void set_interpreteur_first_token(interpreteur inter, char first_token)
{
  inter->first_token = first_token;
}

inter_mode get_interpreteur_mode(interpreteur inter)
{
  return inter->mode;
}
FILE* get_interpreteur_fp(interpreteur inter)
{
    return inter->fp;
}

char* get_interpreteur_input(interpreteur inter)
{
  return inter->input;
}

char get_interpreteur_first_token(interpreteur inter)
{
  return inter->first_token;
}

interpreteur init_inter(void) {
    interpreteur inter = calloc(1,sizeof(*inter));
    if (inter ==NULL)
        ERROR_MSG("impossible d'allouer un nouvel interpreteur");
    return inter;
}

void del_inter(interpreteur inter) {
    if (inter !=NULL)
        free(inter);
}

char* get_next_token(interpreteur inter) {

    char       *token = NULL;
    char       *delim = " \t\n";


    if ( get_interpreteur_first_token(inter) == 0 ) {
        token = strtok_r(get_interpreteur_input(inter), delim, &inter->from);
        set_interpreteur_first_token(inter, 1);
    }
    else {
        token = strtok_r( NULL, delim, &inter->from);
    }
    if ( NULL == token ) {
        set_interpreteur_first_token(inter, 0);
    }

    return token;
}

int is_hexa(char* chaine) {
    int i = 2;
    if (!(chaine!=NULL && strlen(chaine)>2&& chaine[0]=='0' && chaine[1]=='x'))
      return 0;
    while(chaine[i] != '\0')
    {
      if( (!isxdigit(chaine[i++])))
        return 0;
    }
    return 1; //  Youpi tout va bien
}

int is_hexa_format(char* chaine, int n)
{
  if(!is_hexa(chaine))
    return 0;
  return strlen(chaine) - 2 <= n;
}

int is_reg(char* chaine)
{
  int i; // Pour parcourir
  int regNb;  // On va ranger l'entier ici
  char* p = chaine;

  if(chaine[0] != '$' && strcmp(chaine, "hi")
    && strcmp(chaine, "lo") && strcmp(chaine, "pc")) return 0;
  if(chaine[0] == '$')
      p++;
  if(isdigit(*p))
  {
    sscanf(p,"%d",&regNb);
    if(regNb >= 0 && regNb <= 32)
      return 1;
  }
  else
  {
    for(i=0; i < 35;i++)
    {
      if( i < 32)
      {
        if(strcmp(p,nom_registre[i]) == 0)
          return 1;
      }
      else
      {
        if(strcasecmp(p,nom_registre[i]) == 0)
          return 1;
      }
    }
  }
  return 0;
}
int is_readable(segment_s seg)
{
  return get_seg_droit(seg)[0] == 'r';
}

int is_writable(segment_s seg)
{
  return get_seg_droit(seg)[1] == 'w';
}

int is_executable(segment_s seg)
{
  return get_seg_droit(seg)[2] == 'x';
}

int is_number8(char* chaine)
{
  char* p = chaine;
  char* max = "255";
  if(*chaine == '-')
    p++;
  char* p2 = p; //  Va servir pour l'overflow
  for(; *p != '\0';p++) //  On controle que c'est bien un nombre
  {
    if(!isdigit(*p))
      return 0;
  }
  //  On vire les 0
  for(; *p2 == '0' && *p2 != '\0'; p2++){}

  //  On gère les cas évidents
  if(strlen(max) < strlen(p2))
    return 0; //  Overflow évident
  else if(strlen(max) > strlen(p2))
    return 1; // Moins de digit que la valeur max, donc forcément ok

  //  On va faire la comparaison digit après digit de la chaine
  int i;
  for(i = 0; i < strlen(max); i++)
  {
    // On compare digit après digit en commencant par celui de poids fort (le premier)
    if(max[i] < p2[i])
      return 0;
  }

  return 1;
}

int is_number32(char* chaine)
{
  char* p = chaine;
  char* max = "276447232";
  if(*chaine == '-')
    p++;
  char* p2 = p; //  Va servir pour l'overflow
  for(; *p != '\0';p++) //  On controle que c'est bien un nombre
  {
    if(!isdigit(*p))
      return 0;
  }
  //  On vire les 0
  for(; *p2 == '0' && *p2 != '\0'; p2++){}

  //  On gère les cas évidents
  if(strlen(max) < strlen(p2))
    return 0; //  Overflow évident
  else if(strlen(max) > strlen(p2))
    return 1; // Moins de digit que la valeur max, donc forcément ok

  //  On va faire la comparaison digit après digit de la chaine
  int i;
  for(i = 0; i < strlen(max); i++)
  {
    // On compare digit après digit en commencant par celui de poids fort (le premier)
    if(max[i] < p2[i])
      return 0;
  }

  return 1;
}

int get_type(char* chaine) {
    if (is_hexa(chaine))
        return HEXA;
    else if(is_reg(chaine))
        return REG;
    return UNKNOWN;
}

int execute_cmd(interpreteur inter, cpu_s cpu, dico_s d) {
    char cmdStr[MAX_STR];
    memset( cmdStr, '\0', MAX_STR );

    /* gestion des commandes vides, commentaires, etc*/
    if(strlen(get_interpreteur_input(inter)) == 0
            || sscanf(get_interpreteur_input(inter), "%s", cmdStr) == 0
            || strlen(cmdStr) == 0
            || cmdStr[0] == '#') { /* ligne commence par # => commentaire*/
        return CMD_OK_RETURN_VALUE;
    }

    /*on identifie la commande avec un premier appel à get_next_token*/
    char * token = get_next_token(inter);

    if(strcmp(token, "exit") == 0)
        return exitcmd(inter);
    else if(strcmp(token, "disp") == 0)
        return dispCmd(inter, cpu);
    else if(strcmp(token, "set") == 0)
      return setCmd(inter, cpu);
    else if(strcmp(token, "assert") == 0)
      return assertCmd(inter, cpu);
    else if(strcmp(token, "debug") == 0)
      return debugCmd(inter);
    else if(strcmp(token, "resume") == 0)
      return resumeCmd(inter);
    else if(strcmp(token, "load") == 0)
      return loadCmd(inter, cpu);
    else if(strcmp(token, "disasm") == 0)
      return disasmCmd(inter,cpu,d);
    else if(strcmp(token, "run") == 0)
      return runCmd(inter,cpu,d);

    WARNING_MSG("Unknown Command: '%s'\n", cmdStr);
    WARNING_MSG("Known Commands: exit | disp | set | assert | resume | debug | load | disasm");
    return CMD_UNKOWN_RETURN_VALUE;
}


void string_standardise( char* in, char* out ) {
    unsigned int i=0, j;

    for ( j= 0; i< strlen(in); i++ ) {

        /* insert blanks around special characters*/
        if (in[i]==':' || in[i]=='+' || in[i]=='~') {
            out[j++]=' ';
            out[j++]=in[i];
            out[j++]=' ';

        }

        /* remove blanks after negation*/
        else if (in[i]=='-') {
            out[j++]=' ';
            out[j++]=in[i];
            while (isblank((int) in[i+1])) i++;
        }

        /* insert one blank before comments */
        else if (in[i]=='#') {
            out[j++]=' ';
            out[j++]=in[i];
        }
        /* translate tabs into white spaces*/
        else if (isblank((int) in[i])) out[j++]=' ';
        else out[j++]=in[i];
    }
}

int  acquire_line(FILE *fp, interpreteur inter) {
    char* chunk =NULL;

    memset(get_interpreteur_input(inter), '\0', MAX_STR );
    set_interpreteur_first_token(inter, 0);
    if (get_interpreteur_mode(inter)==SCRIPT) {
        // mode fichier
        // acquisition d'une ligne dans le fichier
        chunk =calloc(MAX_STR, sizeof(*chunk));

        char * ret = fgets(chunk, MAX_STR, get_interpreteur_fp(inter) );

        if(ret == NULL) {
            free(chunk);
            return 1;
        }
        // si windows remplace le \r du '\r\n' (fin de ligne windows) par \0
        if(strlen(ret) >1 && ret[strlen(ret) -2] == '\r') {
            ret[strlen(ret)-2] = '\0';
        }
        // si unix remplace le \n par \0
        else if(strlen(ret) >0 && ret[strlen(ret) -1] == '\n') {
            ret[strlen(ret)-1] = '\0';
        }
    }
    else {
        /* mode shell interactif */
        /* on utilise la librarie libreadline pour disposer d'un historique */
        chunk = readline( PROMPT_STRING );
        if (chunk == NULL || strlen(chunk) == 0) {
            /* commande vide... */
            if (chunk != NULL) free(chunk);
            return 1;
        }
        /* ajout de la commande a l'historique, librairie readline */
        add_history( chunk );

    }
    // standardisation de la ligne d'entrée (on met des espaces là ou il faut)
    string_standardise(chunk, get_interpreteur_input(inter));

    free( chunk ); // liberation de la mémoire allouée par la fonction readline() ou par calloc()

    return 0;
}

void usage_ERROR_MSG( char *command ) {
    fprintf( stderr, "Usage: %s [file.emul]\n   If no file is given, executes in Shell mode.", command );
}

int main ( int argc, char *argv[] ) {
    interpreteur inter=init_inter(); /* structure gardant les infos et états de l'interpreteur*/
    cpu_s cpu = init_cpu(nb_seg);
    FILE* fp = NULL;
    set_interpreteur_fp(inter,fp); /* le flux dans lequel les commande seront lues : stdin (mode shell) ou un fichier */
    dico_s dico = build_dico("dico.dic");

    if ( argc > 2 ) {
        usage_ERROR_MSG( argv[0] );
        exit( EXIT_FAILURE );
    }
    if(argc == 2 && strcmp(argv[1], "-h") == 0) {
        usage_ERROR_MSG( argv[0] );
        exit( EXIT_SUCCESS );
    }

    /*par defaut : mode shell interactif */
    fp = stdin;
    set_interpreteur_mode(inter, INTERACTIF);
    if(argc == 2) {
        /* mode fichier de commandes */
        fp = fopen( argv[1], "r" );
        set_interpreteur_fp(inter,fp);
        if ( fp == NULL ) {
            perror( "fopen" );
            exit( EXIT_FAILURE );
        }
        set_interpreteur_mode(inter, SCRIPT);
    }

    /* boucle infinie : lit puis execute une cmd en boucle */
    while ( 1 ) {
      if(get_interpreteur_mode(inter) == SCRIPT)
        fp = get_interpreteur_fp(inter);
      else
        fp = stdin;

      if (acquire_line( fp,  inter)  == 0 ) {
          /* Une nouvelle ligne a ete acquise dans le flux fp*/
          int res = execute_cmd(inter,cpu,dico); /* execution de la commande */

          // traitement des erreurs
          switch(res) {
          case CMD_OK_RETURN_VALUE:
              break;
          case CMD_EXIT_RETURN_VALUE:
              /* sortie propre du programme */
              if ( get_interpreteur_mode(inter) == SCRIPT ) {
                  fclose( fp );
              }
              free_cpu(cpu);
              del_inter(inter);
              dico_del(dico);
              exit(EXIT_SUCCESS);
              break;
          default:
              /* erreur durant l'execution de la commande */
              /* En mode "fichier" toute erreur implique la fin du programme ! */
              if (get_interpreteur_mode(inter) == SCRIPT) {
                  fclose( fp );
                  del_inter(inter);
                  /*macro ERROR_MSG : message d'erreur puis fin de programme ! */
                  ERROR_MSG("ERREUR DETECTEE. Aborts");
              }
              break;
          }
      }
      if( get_interpreteur_mode(inter) == SCRIPT && feof(fp) ) {
          /* mode fichier, fin de fichier => sortie propre du programme */
          DEBUG_MSG("FIN DE FICHIER");
          fclose( fp );
          dico_del(dico);
          del_inter(inter);
          exit(EXIT_SUCCESS);
      }
    }
    /* tous les cas de sortie du programme sont gérés plus haut*/
    ERROR_MSG("SHOULD NEVER BE HERE\n");
}
