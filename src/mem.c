/**
  * @file mem.c
  * @author Mathias Montginoux
  * @brief fichier de mise en place de l'environnement mémoire du mips
  * @Creation 16/09/2019
  * @Update 1/10/2019 by Mathias Reus
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mem.h"

char * nom_registre[] = {"zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "t8", "t9", "k0", "k1", "gp", "sp", "fp", "ra", "HI", "LO", "PC"};

char * nom_segment[] = {".text", ".data", ".bss", "[stack]", "[vsyscall]"};
char * droit_segment[] = {"r-x", "rw-", "rw-", "rw-", "r-x"};
uint32_t Vaddr_segment[] = {0x00005000, 0x00006000, 0x00007000, 0x0f7ff000, 0x0ffff000};
int taille_virt_segment[] = {12, 8, 0, 8388608, 4096};
int taille_re_segment[] = {12, 8, 0, 80, 40};


void set_reg_name(registre_s  reg, char * name){
  reg->name = strdup(name);
}

void set_reg_val(registre_s  reg, int32_t val){
  reg->val = val;
}

char * get_reg_name(registre_s  reg){
  return reg->name;
}

int32_t get_reg_val(registre_s  reg){
  return reg->val;
}

registre_s get_reg(registreTable_s regTab, int numero){
  return regTab+numero;
}

// Initialise le tableau de registre.
void init_reg(registreTable_s reg){
  DEBUG_MSG("Initialisation des registres");
  int i = 0;
  for (i=0;i<35;i++){
    set_reg_name(&reg[i], nom_registre[i]);
    set_reg_val(&reg[i], 0);
  }
}

void free_reg_name(registre_s reg){
  free(reg->name);
}
registre_s reg_locate(registreTable_s reg, char* chaine)
{
  //  La chaine est correcte lorsqu'on rentre dans cette fonction
  int nbReg;
  int i;
  //  On enlève le $ de la chaîne
  if(*chaine == '$')
    chaine++;
  if(isdigit(*chaine))
  {
    sscanf(chaine, "%d", &nbReg);
  }
  else
  {
    for(i=0;i<35;i++)
    {
      if(strcasecmp(chaine,nom_registre[i])==0)
        nbReg = i;
    }
  }

  return get_reg(reg, nbReg);
}
void set_seg_name(segment_s  seg, char * name){
  seg->name = strdup(name);
}

void set_seg_taille(segment_s  seg,int taille){
  seg->taille = taille;
}

void set_seg_droit(segment_s  seg, char * droit){
  seg->droit = strdup(droit);
}

void set_seg_adresse(segment_s  seg, uint32_t adresse){
  seg->adresse = adresse;
}

void set_seg_data(segment_s seg, uint8_t * data){
  seg->data = data;
}

char * get_seg_name(segment_s seg){
  return seg->name;
}

int get_seg_taille(segment_s  seg){
  return seg->taille;
}

char * get_seg_droit(segment_s seg){
  return seg->droit;
}

uint32_t get_seg_adresse(segment_s  seg){
  return seg->adresse;
}

uint8_t * get_seg_data(segment_s seg){
  return seg->data;
}

segment_s seg_locate(RAM_s mem, uint32_t add)
{
  segment_s p;
  for (p = mem; p < mem + nb_seg; p++)  //  On parcourt les segments
  {
    uint32_t addFirst = get_seg_adresse(p);
    uint32_t addLast = get_seg_adresse(p) + get_seg_taille(p);
    if (add >= addFirst && add < addLast) //  Si je suis dans le segment
      return p;
  }
  return NULL; // Je suis NULL part
}

uint32_t get_word(RAM_s mem, uint32_t startAdd)
{
  segment_s seg = seg_locate(mem, startAdd);
  int pos = startAdd - get_seg_adresse(seg);
  uint8_t* data = get_seg_data(seg);
  return (uint32_t) data[pos]*0x01000000 + data[pos+1]*0x00010000 + data[pos+2]*0x00000100 + data[pos+3];
}


void set_word(RAM_s mem, uint32_t val, uint32_t startAdd)
{
  segment_s seg = seg_locate(mem, startAdd);
  int pos = startAdd - get_seg_adresse(seg);
  uint8_t* data = get_seg_data(seg);
  data[pos] = (val & 0xFF000000)/0x01000000;
  data[pos+1] = (val & 0x00FF0000)/0x00010000;
  data[pos+2] = (val & 0x0000FF00)/0x00000100;
  data[pos+3] = val & 0x000000FF;

}

void set_cpu_reg(cpu_s cpu, int num, char *name){
  set_reg_name(&cpu->reg[num], name);
}

void set_cpu_nseg(cpu_s cpu, int nseg){
  cpu->nseg = nseg;
}

int get_cpu_nseg(cpu_s cpu){
  return cpu->nseg;
}

segment_s get_cpu_mem(cpu_s cpu){
  return cpu->mem;
}

registre_s get_cpu_reg(cpu_s  cpu)
{
  return cpu->reg;
}
void alloc_1segment_memoire(segment_s  seg, int taille){
  uint8_t * data = NULL;
  if ( (data=calloc(taille, sizeof(*data))) == NULL){
    WARNING_MSG("Probleme de calloc dans init_segment_memoire");
    exit(EXIT_FAILURE);
  }
  set_seg_taille(seg, taille);
  set_seg_data(seg, data);
}

void free_1segment_memoire(segment_s  seg){
  free(seg->name);
  free(seg->droit);
  free(seg->data);
}

void free_RAM_s(RAM_s r, int nseg){
  int i = 0;
  for(i=0;i<nseg;i++){
    free_1segment_memoire(&r[i]);
  }
}

// La fonction "init_all_segment" permet de définir chaque segments par rapport aux données d'entrées (nom, taille ...)
void init_all_segment(cpu_s cpu, int nseg){
  DEBUG_MSG("Initialisation des segments(init_all_segment)");
  segment_s r = get_cpu_mem(cpu);
  int i = 0;
  for(i=0;i<nseg;i++){
    set_seg_name(&r[i], nom_segment[i]);
    set_seg_droit(&r[i], droit_segment[i]);
    set_seg_adresse(&r[i], Vaddr_segment[i]);
    set_seg_taille(&r[i], taille_re_segment[i]);
    alloc_1segment_memoire(&r[i], taille_re_segment[i]);
  }
}

cpu_s init_cpu(int nseg){
  DEBUG_MSG("Initialisation du cpu");
  cpu_s cpu = calloc(1, sizeof(*cpu));
  set_cpu_nseg(cpu, nseg);
  init_reg(cpu->reg);
  init_all_segment(cpu, nseg);

  return cpu;
}

void free_cpu(cpu_s cpu){
  int i = 0;
  for(i=0;i<35;i++){
    free_reg_name(&cpu->reg[i]);
  }
  free_RAM_s(cpu->mem, nb_seg);
  free(cpu);
}
