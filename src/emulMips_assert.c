#include "emulMips_assert.h"

int assertCmd(interpreteur inter, cpu_s cpu)
{
  DEBUG_MSG("Command: assert");
  char* token;
  int no_args = 1;
  int return_value;

  if((token = get_next_token(inter)) != NULL)
  {
    no_args = 0;
    if(strcmp("word", token) == 0)
      return_value = assertCmdWord(inter, cpu);
    else if(strcmp("byte", token) == 0)
      return_value = assertCmdByte(inter, get_cpu_mem(cpu));
    else
    {
      WARNING_MSG("%22s: parameter byte or word is required after asert", "Invalid argument");
      WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
      WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
      WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
      WARNING_MSG("%22s: a 8 bits or 32 bits unsigned value according to type", "Value");
      WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
      WARNING_MSG("%22s: a 32 bits hexa address", "Address");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }


  if(no_args)
  {
    WARNING_MSG("%22s: parameter byte or word is required after asert", "No argument");
    WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
    WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
    WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
    WARNING_MSG("%22s: a 8 bits or 32 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return_value = CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int assertCmdWord(interpreteur inter, cpu_s cpu)
{
  DEBUG_MSG("Command: assert word");
  char* token;
  char* emptyToken;
  int return_value;

  if((token = get_next_token(inter)) != NULL)
  {
    if(is_hexa_format(token, 32/4) || is_number32(token))
    {
      uint32_t val;
      sscanf(token,"%x", &val);
      token = NULL;

      if((token = get_next_token(inter)) != NULL)
      {
        if(is_reg(token))
          return_value = assertCmdWordReg(get_cpu_reg(cpu), val, token);
        else if(is_hexa_format(token,32/4))
        {
          uint32_t add;
          sscanf(token,"%x", &add);
          return_value = assertCmdWordRAM(get_cpu_mem(cpu), val, add);
        }
        else // Ce n'est pas un registre ou une address valable
        {
          WARNING_MSG("%22s: parameter register or address is required after value", "Invalid argument");
          WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
          WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
          WARNING_MSG("%22s: a 32 bits unsigned value according to type", "Value");
          WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
          WARNING_MSG("%22s: a 32 bits hexa address", "Address");
          return_value = CMD_INVALID_ARGS_RETURN_VALUE;
        }
        if((emptyToken = get_next_token(inter)) != NULL)
        {
          WARNING_MSG("%22s: parameter is not need after address", "Invalid argument");
          return_value = CMD_INVALID_ARGS_RETURN_VALUE;
        }
      }
      else // Il n'y pas de truc après value
      {
        WARNING_MSG("%22s: parameter register or address is required after value", "No argument");
        WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
        WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
        WARNING_MSG("%22s: a 32 bits unsigned value according to type", "Value");
        WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
        WARNING_MSG("%22s: a 32 bits hexa address", "Address");
        return_value = CMD_NO_ARGS_RETURN_VALUE;
      }
    }
    else // Value n'est pas du bon format
    {
      WARNING_MSG("%22s: parameter value is required after word", "Invalid argument");
      WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
      WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
      WARNING_MSG("%22s: a 32 bits unsigned value according to type", "Value");
      WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
      WARNING_MSG("%22s: a 32 bits hexa address", "Address");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }
  else // Il n'y pas de value
  {
    WARNING_MSG("%22s: parameter value is required after word", "No argument");
    WARNING_MSG("%22s: assert word <value> <register>","Register value testing");
    WARNING_MSG("%22s: assert word <value> <address>","Address word testing");
    WARNING_MSG("%22s: a 32 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return_value = CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}


int assertCmdWordReg(registreTable_s regT, uint32_t val, char* chaine)
{
  registre_s reg = reg_locate(regT, chaine);

  int return_value = get_reg_val(reg) != val;
  if(return_value)
    printf("Command assert result: false\n");
  else
    printf("Command assert result: true\n");
  return return_value;
}

int assertCmdWordRAM(RAM_s memmap, uint32_t val, uint32_t add)
{
  segment_s seg;
  if((seg = seg_locate(memmap, add))==NULL)
  {
    WARNING_MSG("%22s: the address is below the first segment (out of bound)", "No argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  int taille = get_seg_taille(seg);
  uint32_t firstAdd = get_seg_adresse(seg);
  int decalage = add - firstAdd;
  uint8_t* data = get_seg_data(seg) + decalage;

  if(decalage > taille - 4)
  {
    WARNING_MSG("%22s: the end of the word is out of bound", "No argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t valMem = 0;
  valMem = *(data)*0x1000000 + *(data+1)*0x10000 + *(data+2)*0x100 + *(data+3);

  int return_value = valMem != val;
  if(return_value)
    printf("Command assert result: false\n");
  else
    printf("Command assert result: true\n");
  return return_value;

}
int assertCmdByte(interpreteur inter, RAM_s memmap)
{
  DEBUG_MSG("Command: assert byte");
  char* token;
  char* emptyToken;
  int return_value;

  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%22s: parameter value is required after byte", "No argument");
    WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
    WARNING_MSG("%22s: a 8 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return CMD_NO_ARGS_RETURN_VALUE;
  }
  DEBUG_MSG("is_number8(token) = %i", is_number8(token));
  if(is_hexa_format(token, 8/4) == 0 && is_number8(token) == 0)
  {
    WARNING_MSG("%22s: parameter value is required after byte", "Invalid argument");
    WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
    WARNING_MSG("%22s: a 8 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return  CMD_INVALID_ARGS_RETURN_VALUE;
  }
  uint8_t val;
  sscanf(token,"%hhx", &val);
  token = NULL;
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%22s: parameter address is required after value", "No argument");
    WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
    WARNING_MSG("%22s: a 8 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if((emptyToken = get_next_token(inter)) != NULL)
  {
    WARNING_MSG("%22s: parameter is not need after address", "Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  if(!is_hexa_format(token, 32/4))
  {
    WARNING_MSG("%22s: parameter address is required after value", "Invalid argument");
    WARNING_MSG("%22s: assert byte <value> <address>","Address byte testing");
    WARNING_MSG("%22s: a 8 bits unsigned value according to type", "Value");
    WARNING_MSG("%22s: a 32 bits hexa address", "Address");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t add;
  sscanf(token,"%x", &add);
  segment_s seg;
  if((seg = seg_locate(memmap, add))==NULL)
  {
    WARNING_MSG("%22s: the address is below the first segment (out of bound)", "Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  int taille = get_seg_taille(seg);
  uint32_t firstAdd = get_seg_adresse(seg);
  int decalage = add - firstAdd;
  if(decalage > taille)
  {
    WARNING_MSG("%22s: the address is out of bound", "Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  uint8_t data = *(get_seg_data(seg)+decalage);

  return_value = data != val;
  if(return_value)
    printf("Command assert result: false\n");
  else
    printf("Command assert result: true\n");
  return return_value;
}
