#include "emulMips_run_instr.h"

int toutvabien(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("Ceci est une fonction test");
  nextPC(cpu);
  return 0;
}

void nextPC(cpu_s cpu)
{
  registre_s reg = get_cpu_reg(cpu);
  int32_t adPC = get_reg_val(reg+35-1);
  set_reg_val(reg+35-1,adPC+4);
}

int instr_AND(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("AND");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  //  On récupère la valeur qui nous intéresse
  int32_t valRs = get_reg_val(reg+word->r.rs);
  int32_t valRt = get_reg_val(reg+word->r.rt);
  //  On fait l'opération AND
  set_reg_val(reg+word->r.rd, valRs & valRt);

  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_ANDI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("ANDI");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  //  On récupère la valeur qui nous intéresse
  int32_t valRs = get_reg_val(reg+word->i.rs);
  int32_t valImm = (int32_t) word->i.imm ;
  //  On fait l'opération AND
  set_reg_val(reg+word->i.rt, valRs & valImm);

  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_BEQ(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BEQ");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  int32_t valRt = get_reg_val(reg+word->i.rt);
  //  On fait le branchement
  if (valRt == valRs)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);

  return CMD_OK_RETURN_VALUE;
}

int instr_BGEZ(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BGEZ");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  //  On fait le branchement
  if (valRs >= 0)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_BGTZ(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BGTZ");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  //  On fait le branchement
  if (valRs > 0)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);

  return CMD_OK_RETURN_VALUE;
}

int instr_BLEZ(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BLEZ");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  //  On fait le branchement
  if (valRs <= 0)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);

  return CMD_OK_RETURN_VALUE;
}

int instr_BLTZ(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BLTZ");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  //  On fait le branchement
  if (valRs < 0)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);

  return CMD_OK_RETURN_VALUE;
}

int instr_BNE(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BNE");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  //  On récupère les valeurs de rs et rt
  int32_t valRs = get_reg_val(reg+word->i.rs);
  int32_t valRt = get_reg_val(reg+word->i.rt);
  //  On fait le branchement
  if (valRt != valRs)
  {
    nInstr->instruction(nInstr, cpu, nInstr);
    set_reg_val(reg+35-1, pc + (((short) word->i.imm) << 2));
  }
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_BREAK(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("BREAK");

  return 0; // À changer lorsque je ferai les breaks points.
}

int instr_J(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("J");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  int32_t pcBig = pc & 0xF000000; // 4 premier bits

  nInstr->instruction(nInstr, cpu, nInstr);
  set_reg_val(reg+35-1, pc + (pcBig + (word->j.target<<2)));

  return CMD_OK_RETURN_VALUE;
}

int instr_JAL(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("JAL");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  int32_t pc = get_reg_val(reg+35-1);
  int32_t pcBig = pc & 0xF000000; // 4 premier bits

  //  Une fois la fonction finit, on ira à cette adresse
  set_reg_val(reg+31, pc + 8);


  nInstr->instruction(nInstr, cpu, nInstr);
  set_reg_val(reg+35-1, pc + (pcBig + (word->j.target<<2)));

  return CMD_OK_RETURN_VALUE;
}

int instr_JALR(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("JALR mwe j'ai pas compris la doc, on en a pas besoin de toute façon");
  return CMD_OK_RETURN_VALUE;
}

int instr_JR(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("JR la même que JALR, gnagnagna Mips16e, je sais pas");
  return CMD_OK_RETURN_VALUE;
}

int instr_LB(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("LB");
  //  On pose les trucs utiles
  segment_s mem = get_cpu_mem(cpu);
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  int32_t rBase = get_reg_val(reg + word->i.rs);
  uint32_t address = (uint32_t) rBase + ((short) word->i.imm);
  segment_s seg = seg_locate(mem, address);
  uint32_t addressStart = get_seg_adresse(seg);
  uint8_t* data = get_seg_data(mem);

  set_reg_val(reg + word->i.rt, (int32_t) data[address - addressStart]);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_LBU(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("LBU");
  //  On pose les trucs utiles
  segment_s mem = get_cpu_mem(cpu);
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  int32_t rBase = get_reg_val(reg + word->i.rs);
  uint32_t address = (uint32_t) rBase + word->i.imm;
  segment_s seg = seg_locate(mem, address);
  uint32_t addressStart = get_seg_adresse(seg);
  uint8_t* data = get_seg_data(mem);

  set_reg_val(reg + word->i.rt, data[address - addressStart]);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_LUI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("LUI");
  //  On pose les trucs utiles
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);

  set_reg_val(reg + word->i.rt, word->i.imm | 0x00000000);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_LW(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("LB");
  //  On pose les trucs utiles
  registre_s reg = get_cpu_reg(cpu);
  segment_s mem = get_cpu_mem(cpu);
  code_s word = get_instr_code(instr);
  uint32_t ad = get_code(word);

  set_reg_val(reg + word->i.rt, (int32_t) get_word(mem, ad));
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_MFHI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("MFHI");
  //  On pose les trucs ok
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  int32_t hi = get_reg_val(reg + 35 - 3);

  set_reg_val(reg + word->r.rd, hi);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_MFLO(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("MFLO");
  //  On pose les trucs ok
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  int32_t lo = get_reg_val(reg + 35 - 2);

  set_reg_val(reg + word->r.rd, lo);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_NOP(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("NOP");
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_OR(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("OR");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  //  On récupère la valeur qui nous intéresse
  int32_t valRs = get_reg_val(reg+word->r.rs);
  int32_t valRt = get_reg_val(reg+word->r.rt);
  //  On fait l'opération OR
  set_reg_val(reg+word->r.rd, valRs | valRt);

  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_ORI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("ORI");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  //  On récupère la valeur qui nous intéresse
  int32_t valRs = get_reg_val(reg+word->i.rs);
  int32_t valImm = (int32_t) word->i.imm ;
  //  On fait l'opération OR
  set_reg_val(reg+word->i.rt, valRs | valImm);

  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SB(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SB");
  // La base
  registre_s reg = get_cpu_reg(cpu);
  segment_s mem = get_cpu_mem(cpu);
  code_s word = get_instr_code(instr);
  //  Les adresses qui nous intéresse
  uint32_t adresse = (uint32_t) get_reg_val(reg + word->i.rs) + ((short) word->i.imm);
  segment_s seg = seg_locate(mem, adresse);
  uint8_t* data = get_seg_data(seg);
  uint32_t addressStart = get_seg_adresse(seg);

  int8_t value = get_reg_val(reg + word->i.rt) & 0x000000FF;

  data[adresse - addressStart] = value;
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SEB(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SEB");
  // La base
  registre_s reg = get_cpu_reg(cpu);
  segment_s mem = get_cpu_mem(cpu);
  code_s word = get_instr_code(instr);
  //  Les adresses qui nous intéresse
  uint32_t adresse = (uint32_t) get_reg_val(reg + word->i.rs) + ((short) word->i.imm);
  segment_s seg = seg_locate(mem, adresse);
  uint8_t* data = get_seg_data(seg);
  uint32_t addressStart = get_seg_adresse(seg);

  uint8_t value = (uint8_t) get_reg_val(reg + word->i.rt) & 0x000000FF;

  data[adresse - addressStart] = value;
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SLL(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SLL");
  //  La base
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  //  On récupère les registres
  int32_t GPRrt = get_reg_val(reg + word->r.rt);

  set_reg_val(reg + word->r.rd, GPRrt << word->r.sa);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SLT(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SLT");

  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);

  int32_t GPRrs = get_reg_val(reg + word->r.rs);
  int32_t GPRrt = get_reg_val(reg + word->r.rt);

  set_reg_val(reg + word->r.rd, GPRrs < GPRrt);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SLTI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SLTI");

  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);

  int32_t GPRrs = get_reg_val(reg + word->i.rs);

  set_reg_val(reg + word->i.rt, GPRrs < ((short) word->i.imm));
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SLTUI(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SLTUI");

  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);

  int32_t GPRrs = get_reg_val(reg + word->i.rs);

  set_reg_val(reg + word->i.rt, GPRrs < ((short) word->i.imm));
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SLTU(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SLTU");

  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);

  int32_t GPRrs = get_reg_val(reg + word->r.rs);
  uint32_t GPRrt = (uint32_t) get_reg_val(reg + word->r.rt);

  set_reg_val(reg + word->r.rd, GPRrs < GPRrt);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SRA(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SRA");
  //  La base
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  //  On récupère les registres
  int32_t GPRrt = get_reg_val(reg + word->r.rt);
  //  Pour faire un décalage arithmétique
  uint8_t sign = get_reg_val(reg + word->r.rd) >> 31;
  int32_t comp = 0;
  if(sign)
  {
    int i;
    for(i=0; i < word->r.sa;i++)
      comp = comp + (sign << (31-i));
  }

  set_reg_val(reg + word->r.rd, (GPRrt >> word->r.sa) + comp );
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}


int instr_SRL(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SRL");
  //  La base
  registre_s reg = get_cpu_reg(cpu);
  code_s word = get_instr_code(instr);
  //  On récupère les registres
  int32_t GPRrt = get_reg_val(reg + word->r.rt);

  set_reg_val(reg + word->r.rd, GPRrt >> word->r.sa);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SW(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SW");
  // La base
  registre_s reg = get_cpu_reg(cpu);
  segment_s mem = get_cpu_mem(cpu);
  code_s word = get_instr_code(instr);
  //  Les adresses qui nous intéresse
  uint32_t adresse = (uint32_t) get_reg_val(reg + word->i.rs) + ((short) word->i.imm);
  uint32_t value = (uint32_t) get_reg_val(reg + word->i.rt);

  set_word(mem, value & 0x000000FF, adresse);
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

int instr_SYSCALL(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("SYSCALL");
  registre_s reg = get_cpu_reg(cpu);
  set_reg_val(reg+35-1, 0); // Moyen comme technique pour faire sortir le programme ...
  //  On pourrait mettre une valeur de retour spécifique pour signaler les syscall
  return CMD_OK_RETURN_VALUE;
}

int instr_XOR(instruction_s instr, cpu_s cpu, instruction_s nInstr)
{
  DEBUG_MSG("XOR");
  //  On définit ceux qu'on a besoin !
  code_s word = get_instr_code(instr);
  registre_s reg = get_cpu_reg(cpu);
  //  On récupère la valeur qui nous intéresse
  int32_t valRs = get_reg_val(reg+word->r.rs);
  int32_t valRt = get_reg_val(reg+word->r.rt);
  //  On fait l'opération XOR
  set_reg_val(reg+word->r.rd, valRs ^ valRt);

  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

#define OVERFLOW_RETURN_VALUE 5
/* Vérification d'un overflow pour une addition de nombre non signés sur 32bits */
int overflow_add_32(int32_t a, int32_t b){
  int32_t max = 2147483647 ;
  int32_t min = -2147483648 ;
  if ( (max - a < b) && (min - a > b)  ){
    //OVERFLOW
    return OVERFLOW_RETURN_VALUE;
  }
  return CMD_OK_RETURN_VALUE;
}

int overflow_sub_32(int32_t a, int32_t b){
  int32_t max = 2147483647 ;
  int32_t min = -2147483648 ;
  if ( (a > max + b) && ( a < min + b)  ){
    //OVERFLOW
    return OVERFLOW_RETURN_VALUE;
  }
  return CMD_OK_RETURN_VALUE;
}

int overflow_mult_32(int32_t a, int32_t b){
  int32_t x = a*b;
  if ( a!=0 && x/a != b){
    //OVERFLOW
    return OVERFLOW_RETURN_VALUE;
  }
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction ADD
*/
int instr_ADD(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction ADD");
  code_s code = get_instr_code(inst);
  registre_s rd, rs, rt;
  // On récupère les arguments
  rd = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rd] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  // On vérifie si il y a un overflow ou pas
  int overflow = overflow_add_32( get_reg_val(rs), get_reg_val(rt) );
  if ( overflow != 0 ){
    return overflow;
  }
  set_reg_val(rd, get_reg_val(rs) + get_reg_val(rt) );
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction ADDI
*/
int instr_ADDI(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction ADDI");
  code_s code = get_instr_code(inst);
  registre_s rt, rs;
  uint16_t immediate;

  // On récupère les arguments
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->i.rt] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->i.rs] );
  immediate = (short) code->i.imm;
  // On vérifie si il y a un overflow ou pas
  int overflow = overflow_add_32( get_reg_val(rs), (int32_t)immediate );
  if ( overflow != 0 ){
    return overflow;
  }
  set_reg_val(rt, get_reg_val(rs) + (int32_t)immediate );
  nextPC(cpu);
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction ADDIU
*/
int instr_ADDIU(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction ADDIU");
  code_s code = get_instr_code(inst);
  registre_s rt, rs;
  uint16_t immediate;

  // On récupère les arguments
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->i.rt] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->i.rs] );
  immediate = (short) code->i.imm;

  set_reg_val(rt, get_reg_val(rs) + (int32_t)immediate );
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction ADDU
*/
int instr_ADDU(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction ADD");
  code_s code = get_instr_code(inst);
  registre_s rd, rs, rt;
  // On récupère les arguments
  rd = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rd] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  set_reg_val(rd, get_reg_val(rs) + get_reg_val(rt));
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction SUB
*/
int instr_SUB(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction SUB");
  code_s code = get_instr_code(inst);
  registre_s rd, rs, rt;
  // On récupère les arguments
  rd = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rd] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  // On vérifie si il y a un overflow ou pas
  int overflow = overflow_sub_32( get_reg_val(rs), get_reg_val(rt) );
  if ( overflow != 0 ){
    return overflow;
  }

  set_reg_val(rd, get_reg_val(rs) - get_reg_val(rt));
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction SUBU
*/
int instr_SUBU(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction SUBU");
  code_s code = get_instr_code(inst);
  registre_s rd, rs, rt;
  // On récupère les arguments
  rd = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rd] );
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  set_reg_val(rd, get_reg_val(rs) - get_reg_val(rt));
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction MULT
*/
int instr_MULT(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction MULT");
  code_s code = get_instr_code(inst);
  registre_s rs, rt;
  // On récupère les arguments
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  int overflow = overflow_mult_32(get_reg_val(rs), get_reg_val(rt));
  if(overflow!=0){
    //OVERFLOW
    WARNING_MSG("overflow in MULT");
    return overflow;
  }
  int64_t res = (int64_t)get_reg_val(rs)*(int64_t)get_reg_val(rt);

  int32_t val_LO = (int32_t)(0xffffffff)&res;
  res = ( (0xffffffff00000000)&res )>>32;
  int32_t val_HI = (int32_t)res;

  set_reg_val(get_cpu_reg(cpu)+32, val_HI); // Dans registre HI
  set_reg_val(get_cpu_reg(cpu)+33, val_LO); // Dans registre LO
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}

/*########################################################################*/
/*
  Instruction DIV
*/
int instr_DIV(instruction_s inst, cpu_s cpu, instruction_s nInstr){
  DEBUG_MSG("Instruction DIV");
  code_s code = get_instr_code(inst);
  registre_s rs, rt;
  // On récupère les arguments
  rs = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rs] );
  rt = reg_locate( get_cpu_reg(cpu), nom_registre[code->r.rt] );

  // Division par zero ...
  if( get_reg_val(rt)==0 ){
    WARNING_MSG("Divide by zero is a crime");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  set_reg_val(get_cpu_reg(cpu)+32, get_reg_val(rs)%get_reg_val(rt)); // Reste div dans registre HI
  set_reg_val(get_cpu_reg(cpu)+33, get_reg_val(rs)/get_reg_val(rt)); // Quotient div dans registre LO
  set_reg_val(get_cpu_reg(cpu)+34, get_reg_val(get_cpu_reg(cpu)+34) + 4); // Mise à jour de PC
  return CMD_OK_RETURN_VALUE;
}
