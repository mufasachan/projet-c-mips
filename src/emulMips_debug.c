#include "emulMips_debug.h"

int debugCmd(interpreteur inter)
{
  if(get_next_token(inter) != NULL)
  {
    WARNING_MSG("Invalid argument: parameter is not needed after debug");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  if(get_interpreteur_mode(inter) == INTERACTIF)
    printf("Nothing happenned, you are already on interactive mode !\n");
  else
    set_interpreteur_mode(inter, INTERACTIF);
  return 0;
}
