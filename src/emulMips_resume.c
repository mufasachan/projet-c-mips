#include "emulMips_resume.h"

int resumeCmd(interpreteur inter)
{
  if(get_next_token(inter) != NULL)
  {
    WARNING_MSG("Invalid argument: parameter is not needed after resume");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  if(get_interpreteur_fp(inter) == NULL)
  {
    WARNING_MSG("There is no script set basically");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  if(get_interpreteur_mode(inter) == SCRIPT)
    printf("Nothing happenned, you are already on script mode !\n");
  else
    set_interpreteur_mode(inter, SCRIPT);

  return 0;
}
