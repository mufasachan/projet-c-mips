#include "emulMips_disp.h"

int dispCmd(interpreteur inter, cpu_s cpuMips)
{
  DEBUG_MSG("Command: disp");
  char* token;
  int no_args = 1;
  int return_value;

  if ((token = get_next_token(inter))!=NULL)
  {
    no_args = 0;
    if (strcmp(token,"mem") == 0)
      return_value = dispCmdRAM(inter, get_cpu_mem(cpuMips));
    else if(strcmp(token,"reg") == 0)
      return_value = dispCmdReg(inter, get_cpu_reg(cpuMips));
    else
    {
      WARNING_MSG("%16s: parameter mem or reg is required after disp", "Invalid argument");
      WARNING_MSG("%16s: disp mem <range>","Syntax");
      WARNING_MSG("%16s: disp mem map","Syntax");
      WARNING_MSG("%16s: disp reg <register>","Syntax");
      WARNING_MSG("%16s: 2 32bits hexa addresses separated by a \':\'", "Range");
      WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
      WARNING_MSG("%16s: \'all\' to display every register","Register");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }

  if(no_args) {
    WARNING_MSG("%11s: parameter mem or reg is required after disp", "No argument");
    WARNING_MSG("%11s: disp mem <range>","Syntax");
    WARNING_MSG("%11s: disp mem map","Syntax");
    WARNING_MSG("%11s: disp reg <register>","Syntax");
    WARNING_MSG("%11s: 2 32bits hexa addresses separated by a \':\'", "Range");
    WARNING_MSG("%11s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
    WARNING_MSG("%11s: \'all\' to display every register","Register");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int dispCmdReg(interpreteur inter, registreTable_s regT)
{
  DEBUG_MSG("Command: disp reg");
  char* token;
  int no_args = 1;
  int compteur = 1;
  int return_value = CMD_OK_RETURN_VALUE;

  while((token = get_next_token(inter))!= NULL
    && return_value == CMD_OK_RETURN_VALUE)
  {
      no_args = 0;
      if(is_reg(token))
        return_value = dispCmdRegOne(regT, token, &compteur);
      else if (strcmp(token,"all")==0)
        return_value = dispCmdRegAll(regT);
      else
      {
        WARNING_MSG("%16s: parameter register is required after reg", "Invalid argument");
        WARNING_MSG("%16s: disp reg <register>","Syntax");
        WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
        WARNING_MSG("%16s: \'all\' to display every register","Register");
        return_value = CMD_INVALID_ARGS_RETURN_VALUE;
      }
  }
  printf("\n");

  if(no_args) {
    WARNING_MSG("%11s: parameter register is required after reg", "No argument");
    WARNING_MSG("%11s: disp reg <register>","Syntax");
    WARNING_MSG("%11s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc, all}","Register");
    WARNING_MSG("%11s: \'all\' to display every register","Register");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int dispCmdRegOne(registreTable_s regT, char* chaine, int* compteur_ptr)
{
  registre_s reg = reg_locate(regT,chaine);
  //  On affiche
  printf("%4s : 0x%08x", get_reg_name(reg), get_reg_val(reg));

  //  On insère un saut de ligne si c'est le 4ème appel de la fonction
  if((*compteur_ptr)++ == 4)
  {
    printf("\n");
    *compteur_ptr = 1;
  }

  return CMD_OK_RETURN_VALUE;

}


int dispCmdRegAll(registreTable_s regT)
{
  int i,j=0;

  for(i = 0; i < 35; i++)
  {
    if(j++>3)
    {
      j=1;
      printf("\n");
    }
    registre_s reg = get_reg(regT, i);
    printf("%4s : 0x%08x", get_reg_name(reg), get_reg_val(reg));
  }

  return CMD_OK_RETURN_VALUE;
}

int dispCmdRAM(interpreteur inter, RAM_s memmap)
{
    DEBUG_MSG("Command: disp mem");
    char* token;
    int no_args = 1;
    int no_more_map_bro = 1;
    int return_value = CMD_OK_RETURN_VALUE;
    while(no_more_map_bro
      && ((token = get_next_token(inter) )!= NULL)
      && return_value == CMD_OK_RETURN_VALUE)
    {
      no_args = 0;
      if(strcmp("map",token)==0)
      {
        no_more_map_bro = 0;
        return_value = dispCmdRAMMap(inter, memmap);
      }
      else if(is_hexa_format(token,32/4))
      {
        return_value = dispCmdRAMPlage(inter, memmap, token);
      }
      else
      {
        WARNING_MSG("%16s: parameter map or range is required after mem", "Invalid argument");
        WARNING_MSG("%16s: disp mem <range>","Syntax");
        WARNING_MSG("%16s: disp mem map","Syntax");
        WARNING_MSG("%16s: 2 32bits hexa addresses separated by a \':\'", "Range");
        return_value = CMD_INVALID_ARGS_RETURN_VALUE;
      }

    }

    if(no_args) {
      WARNING_MSG("%11s: parameter map or range is required after mem", "No argument");
      WARNING_MSG("%11s: disp mem <range>","Syntax");
      WARNING_MSG("%11s: disp mem map","Syntax");
      WARNING_MSG("%11s: 2 32bits hexa addresses separated by a \':\'", "Range");
      return CMD_NO_ARGS_RETURN_VALUE;
    }

    return return_value;
}

int dispCmdRAMMap(interpreteur inter, RAM_s memmap)
{
  int i;
  char* emptyToken;

  if((emptyToken = get_next_token(inter)) != NULL)
  {
    WARNING_MSG("Invalid argument: parameter is not expected after map");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  printf("Virtual memory map (8 segments)\n");
  //  On va s'implement parcourir le memmap
  for(i = 0;i< nb_seg;i++)
  {
    printf("%-12s", get_seg_name(memmap+i));
    printf("%-5s", get_seg_droit(memmap+i));
    printf("Vaddr: 0x%08x", get_seg_adresse(memmap+i));
    printf("  Size: %i bytes\n", get_seg_taille(memmap+i));
  }

  return CMD_OK_RETURN_VALUE;
}

int dispCmdRAMPlage(interpreteur inter, RAM_s memmap, char* chaine)
{
  char* token;
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter range is required after mem", "No argument");
    WARNING_MSG("%11s: disp mem <range>","Syntax");
    WARNING_MSG("%11s: 2 32bits hexa addresses separated by a \':\'", "Range");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if(strcmp(":",token))
  {
    WARNING_MSG("%16s: missing \':\' to indicate the address range", "Invalid argument");
    WARNING_MSG("%16s: disp mem <range>","Syntax");
    WARNING_MSG("%16s: 2 32bits hexa addresses separated by a \':\'", "Range");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%16s: missing end of range address", "Invalid argument");
    WARNING_MSG("%16s: disp mem <range>","Syntax");
    WARNING_MSG("%16s: 2 32bits hexa addresses separated by a \':\'", "Range");
    return CMD_NO_ARGS_RETURN_VALUE;
  }


  if(!(is_hexa_format(token, 32/4)))
  {
    WARNING_MSG("%16s: parameter range is required after mem", "Invalid argument");
    WARNING_MSG("%16s: disp mem <range>","Syntax");
    WARNING_MSG("%16s: 2 32bits hexa addresses separated by a \':\'", "Range");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t hex1, hex2;
  sscanf(chaine, "%x", &hex1);
  sscanf(token, "%x", &hex2);
  //  Ce sont des mots de 32 bits donc 4 octers de mémoire
  //  La doc nous dit "Affichage de 16 octets au maximum"
  int longueurPlage = hex2 - hex1;
  if (longueurPlage < 0)
  {
    WARNING_MSG("%16s: the range is invalid","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  //  On repère à quel un registre il appartient
  segment_s p;
  if((p = seg_locate(memmap, hex1)) ==NULL)
  {
    WARNING_MSG("%16s: address is below the first segment (out of bound)","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  if(!is_readable(p))
  {
    WARNING_MSG("%16s: the memory segment %s is not readable", "Invalid argument", get_seg_name(p));
    WARNING_MSG("%16s: please type disp mem map for more details about segments' authorization","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  int tailleSeg = get_seg_taille(p);
  uint32_t firstAddressSeg = get_seg_adresse(p);
  //  On détermine si hex1 est bien alloué
  int decalage = hex1 - firstAddressSeg;
  if(decalage > tailleSeg)
  {
    WARNING_MSG("%16s: the first address is out of bound","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  //  On détermine si on est pas trop grand.
  //  Si la valeur de début de plage est la même que le début du segment, cool
  //  Sinon il faut compenser la différence entre le début de la plage et le début du segment.
  int longueurMax = tailleSeg - decalage;
  if(longueurPlage > longueurMax)
  {
    WARNING_MSG("%16s: the second address is out of bound","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  Finalement on affiche
  uint8_t* pAdd = get_seg_data(p+decalage); // On se place au début
  int i, j=0;
  printf("0x%08x ",hex1);
  for(i=0;i < longueurPlage;i++)
  {
    if(j++ > 15)
    {
      j = 1;
      printf("\n0x%08x ", hex1+i);
    }
    printf(" %02x", *pAdd);
    pAdd++;
  }
  printf("\n");
  return CMD_OK_RETURN_VALUE;
}
