#include <stdio.h>
#include <stdlib.h>
#include "emulMips_disasm.h"

int disasmCmd(interpreteur inter, cpu_s cpu, dico_s d)
{
  DEBUG_MSG("Command: disasm");
  char* token;
  uint32_t add1; // adresse de départ
  int plage; // 1 si plage , 0 si offset
  segment_s mem = get_cpu_mem(cpu);

  // Adresse 1
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter hexa 32 bits unsgined  is required after disasm", "No argument");
    WARNING_MSG("%11s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%11s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%11s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%11s: integer offset value","Offset");
    return CMD_NO_ARGS_RETURN_VALUE;
  }
  // Adresse 1 : Hexa 32 ?
  if(!is_hexa_format(token,32/4))
  {
    WARNING_MSG("%16s: parameter hexa 32 bits unsigned  is required after disasm", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  Set adresse 1
  sscanf(token,"%x",&add1);

  //  Adresse dans .text
  segment_s segDepart = seg_locate(mem,add1);
  if (segDepart == NULL)
  {
    WARNING_MSG("%16s: the first address is below the .text segment", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  if(strcmp(".text",get_seg_name(segDepart)))
  {
    WARNING_MSG("%16s: address is not in .text", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  // Adresse 1 multiple de 4
  if(add1%4)
  {
    WARNING_MSG("%16s: address should be a multiple of 4", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  Séparateur
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter \':\' or \'+\' is required after address", "No argument");
    WARNING_MSG("%11s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%11s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%11s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%11s: integer offset value","Offset");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if(*token == ':') plage = 1;
  else if (*token == '+') plage = 0;
  else
  {
    WARNING_MSG("%16s: parameter \':\' or \'+\' is required after disasm", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  // Adresse ou offset
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter address or offset is required after \':\' or \'+\'", "No argument");
    WARNING_MSG("%11s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%11s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%11s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%11s: integer offset value","Offset");
    return CMD_NO_ARGS_RETURN_VALUE;
  }
  if((plage == 1 && !is_hexa_format(token,32/4)) || (plage == 0 && (!is_hexa_format(token,32/4) && !is_number32(token))))
  {
    WARNING_MSG("%16s: parameter address or offset is required after \':\' or \'+\'", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  Pas de truc après qui servent à rien
  if(get_next_token(inter) != NULL)
  {
    WARNING_MSG("%16s: last parameter is not needed", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  //  Que ça soit par offset ou direct avec une seconde adresse
  //  on va faire une adresse de sortie.
  uint32_t add2;
  if(plage == 1)
    sscanf(token,"%x",&add2);
  else
  {
    uint32_t offset;
    sscanf(token, "%i", &offset);
    add2 = add1 + offset;
  }
  // Adresse 2 multiple de 4
  if(add2%4)
  {
    WARNING_MSG("%16s: second address should be a multiple of 4", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  if(add2 - add1 <= 0)
  {
    WARNING_MSG("%16s: address range is not correct", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  int longueur = add2 - add1;
  if(add1 - get_seg_adresse(mem) + longueur > get_seg_taille(mem))
  {
    WARNING_MSG("%16s: address range is too wide", "Invalid argument");
    WARNING_MSG("%16s: disasm <address>:<address>","Syntax");
    WARNING_MSG("%16s: disasm <address>+<offset>","Syntax");
    WARNING_MSG("%16s: hexa 32 bits unsgined", "Address");
    WARNING_MSG("%16s: integer offset value","Offset");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  int i;
  for(i = 0; i < longueur; i = i + 4)
  {
    printf("0x%08x :: ", add1 + i);
    uint32_t val = 0x00000000;
    int j; // On va récupérer le mot
    for(j = 3; j >= 0;j--) val = val + (*(get_seg_data(segDepart)+i+3-j) << 8*j);
    code_s word = build_code(val);
    // code_swap(word);

    // Cherche dans le dico son alter ego et charge word des bons trucs
    instruction_s instr = get_instru(word, d);
    if (instr == NULL)
    {
      WARNING_MSG("Corrupted binary file: instruction has not been found in the dictionnary");
      return CMD_INVALID_ARGS_RETURN_VALUE;
    }
    disasmCmd_affichage(instr,add1);// Affichage de word correct

  }

  return CMD_OK_RETURN_VALUE;
}

instruction_s get_instru(code_s word, dico_s d)
{
  int i;
  instruction_s instr = NULL;
  for(i = 0; i < NB_INSTR; i++)
  {
    if((get_code(word) & get_dico_masque(d+i)) == get_code(get_dico_code(d+i)))
    {
      instr = instru_from_dico(word, d+i);
      return instr;
    }
  }
  return instr;
}

void disasmCmd_affichage(instruction_s instr, uint32_t add1)
{
  printf("0x%08x   %s", get_code(get_instr_code(instr)), get_instr_nom(instr));
  instr_affichage(instr);
  printf("\n");
}
