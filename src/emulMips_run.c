#include "emulMips_run.h"

int runCmd(interpreteur inter, cpu_s cpu, dico_s dico)
{
  DEBUG_MSG("Command: run");
  char* token;

  if(( token = get_next_token(inter)) == NULL)
    return runCmd_instr(cpu, dico);

  // L'utilisateur spécifie une adresse d'entrée
  if(!is_hexa_format(token, 32/4))
  {
    WARNING_MSG("%16s: parameter address could be added after run", "Invalid argument");
    WARNING_MSG("%16s: run [<address>]","Syntax");
    WARNING_MSG("%16s: a 32 bits hexa address, value loaded in PC. Default is current value of PC", "Address");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t startSeg;
  sscanf(token, "%x", &startSeg);
  // L'adresse est un multiple de 4
  if(startSeg % 4 != 0)
  {
    WARNING_MSG("%16s: parameter address should be a multiple of 4", "Invalid argument");
    WARNING_MSG("%16s: run [<address>]","Syntax");
    WARNING_MSG("%16s: a 32 bits hexa address, value loaded in PC. Default is current value of PC", "Address");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  // L'adresse est bien dans le segment data
  segment_s seg = seg_locate(get_cpu_mem(cpu), startSeg);
  if(seg == NULL || !strcmp(".data",get_seg_name(seg)))
  {
    WARNING_MSG("%16s: parameter address should be located in .text ", "Invalid argument");
    WARNING_MSG("%16s: run [<address>]","Syntax");
    WARNING_MSG("%16s: a 32 bits hexa address, value loaded in PC. Default is current value of PC", "Address");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  Si tout se passe bien alors on charge la valeur dans le PC
  set_reg_val(get_cpu_reg(cpu)+35-1,startSeg);
  return runCmd_instr(cpu,dico);
}

int runCmd_instr(cpu_s cpu, dico_s dico)
{
  segment_s mem = get_cpu_mem(cpu);
  registre_s reg = get_cpu_reg(cpu);

  //  On récupère la valeur de PC
  int32_t adPC = get_reg_val(reg+35-1);
  segment_s seg = seg_locate(mem, adPC);

  int valRetour; // Valeur de retour
  while( seg != NULL)
  {
    DEBUG_MSG("PC: 0x%08x", adPC);
    code_s word = build_code(get_word(mem,(uint32_t) adPC));
    instruction_s instr = get_instru(word, dico);

    //  On prévoit l'instruction souvent dans le cas des branchements et sauts
    code_s nWord = NULL;
    instruction_s nInstr = NULL;
    if (seg_locate(mem, adPC +4) != NULL)
    {
      nWord = build_code(get_word(mem,(uint32_t) adPC+4));
      nInstr = get_instru(nWord, dico);
    }

    if ((valRetour = instr->instruction(instr, cpu, nInstr)) != 0)
      return valRetour;

    adPC = get_reg_val(reg+35-1);
    seg = seg_locate(mem, adPC);

    code_del(word);
    code_del(nWord);
  }
  return CMD_OK_RETURN_VALUE;
  }
