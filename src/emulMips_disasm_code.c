#include "emulMips_disasm_code.h"
#include <stdlib.h>
#include <stdio.h>
#include <mem.h>

code_s build_code(uint32_t c)
{
  code_s codeV = calloc(1,sizeof(*codeV));
  codeV->code = c;
  return codeV;
}

void code_del(code_s c)
{
  free(c);
}
uint32_t get_code(code_s codeV)
{
  return codeV->code;
}

void set_code(code_s codeV, uint32_t c)
{
  codeV->code = c;
}

void code_swap(code_s c)
{
  uint32_t code = get_code(c);
  code = (code & 0xFF) << 24
        | (code & 0xFF000000)>>24
        | (code & 0xFF0000)>>8
        | (code & 0xFF00)<<8;
  set_code(c,code);
}

code_s get_instr_code(instruction_s i)
{
  return i->code;
}

char get_instr_type(instruction_s i)
{
  return i->type;
}

char* get_instr_nom(instruction_s i)
{
  return i->nom;
}

char** get_instr_op(instruction_s i)
{
  return i->op;
}

int get_instr_nbOp(instruction_s i)
{
  return i->nbOp;
}

void instr_affichage(instruction_s instr)
{
  instr->affichage(instr);
}

void instr_affichage_LB(instruction_s instr)
{
  code_s code  = get_instr_code(instr);
  printf(" $%s, %x($%s)", nom_registre[code->i.rt], code->i.imm, nom_registre[code->i.rs]);
}
void instr_affichage_classique(instruction_s instr)
{
  int n = get_instr_nbOp(instr);
  int i;
  code_s code  = get_instr_code(instr);
  for(i = 0; i < n; i++)
  {
    char* op = get_instr_op(instr)[i];
    if( get_instr_type(instr) == 'R')
    {
      if(strcmp(op,"rd") == 0) printf(" $%s", nom_registre[code->r.rd]);
      else if (strcmp(op,"rs") == 0) printf(" $%s", nom_registre[code->r.rs]);
      else if (strcmp(op, "rt") == 0) printf(" $%s", nom_registre[code->r.rt]);
      else if (strcmp(op, "sa") == 0) printf(" %i", code->r.sa);
    }
    else if (get_instr_type(instr) == 'J')
      printf(" 0x%x", code->j.target << 2);
    else // Forcément un type I
    {
      if(strcmp(op,"rs") == 0) printf(" $%s", nom_registre[code->i.rs]);
      else if(strcmp(op,"rt") == 0) printf(" $%s", nom_registre[code->i.rt]);
      else if(strcmp(op,"immediate") == 0) printf(" %i", code->i.imm);
      else if(strcmp(op,"offset") == 0) printf(" %i", (short) (code->i.imm << 2)%0x10000);
    }
    if (i != n - 1) printf(",");
  }
}
