#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "common/notify.h"

#include "emulMips_disasm_dico.h"

#include "emulMips_run_instr.h"

dico_s build_dico(char* txtFile)
{
  DEBUG_MSG("Dictionnary loading");
  dico_s d = calloc(NB_INSTR,sizeof(*d)); // Le dico
  FILE* text = fopen(txtFile, "r"); // Le fichier dico
  char line[100]; // La ligne courante en lecture
  char* token; // Chaque bout qui nous intéresse
  int i = 0; // variable de parcour du dico
  int compteurOperand; // incrément opérande pour chaque ligne
  while (fgets(line, 100, text) != NULL)
  {
    char* *op = calloc(3,sizeof(*op)); // Le tableau temporaire
    compteurOperand = 0; // Au début on a a priori 0 opérande
    //  Nom de l'instruction
    token = strtok(line, " ");
    (d+i)->nom = strdup(token);

    // Attribution de la fonction d'affichage en fonction du nom
    if(strcmp(token, "LB") == 0
      || strcmp(token, "LBU") == 0
      || strcmp(token, "LW") == 0
      || strcmp(token, "SB") == 0
      || strcmp(token, "SW") == 0)
      (d+i)->affichage = instr_affichage_LB;
    else
      (d+i)->affichage = instr_affichage_classique;

    // Attribution de la fonction instruction en fonction du nom
    moche((d+i));

    //  Le code de l'instruction
    token = strtok(NULL," ");
    uint32_t tmpCode;
    sscanf(token,"%x",&tmpCode);
    (d+i)->codeV = build_code(tmpCode);

    // Le masque de l'instruction
    token = strtok(NULL, " ");
    sscanf(token,"%x",&(d+i)->masque);

    //  Les opérandes et leur nombres
    while((token = strtok(NULL, " ")) != NULL)
    {
      op[compteurOperand] = strdup(token);
      if(op[compteurOperand][strlen(token) - 1] == '\n')
        op[compteurOperand][strlen(token) - 1] = '\0';
      compteurOperand++;
    }
    (d+i)->op = op;
    (d+i)->nbOp = compteurOperand;

    //  Attribution du type en fonction des opérandes
    if (compteurOperand == 0) (d+i)->type = 'R';
    else if(strcmp(op[0],"target") == 0) (d+i)->type = 'J';
    else if (compteurOperand == 2)
    {
      if (strcmp(op[1],"immediate") == 0
      || (strcmp(op[1],"offset") == 0 )) (d+i)->type = 'I';
    }
    else if (compteurOperand == 3)
    {
      if (strcmp(op[2],"immediate") == 0
      || (strcmp(op[1],"offset") == 0)
      || (strcmp(op[2],"offset") == 0)) (d+i)->type = 'I';
    }
    if((d+i)->type != 'I' && (d+i)->type != 'J') (d+i)->type = 'R';

    i++; // On passe au mot suivant dans le dico
  }
  fclose(text);
  return d;
}

void moche(dico_s dico)
{
  if (!strcmp("AND", dico->nom))
    dico->instruction = instr_AND;
  else if (!strcmp("ANDI", dico->nom))
    dico->instruction = instr_ANDI;
  else if (!strcmp("BEQ", dico->nom))
    dico->instruction = instr_BEQ;
  else if (!strcmp("BGEZ", dico->nom))
    dico->instruction = instr_BGEZ;
  else if (!strcmp("BNE", dico->nom))
    dico->instruction = instr_BNE;
  else if (!strcmp("BGTZ", dico->nom))
    dico->instruction = instr_BGTZ;
  else if (!strcmp("BLEZ", dico->nom))
    dico->instruction = instr_BLEZ;
  else if (!strcmp("BLTZ", dico->nom))
    dico->instruction = instr_BLTZ;
  else if (!strcmp("J", dico->nom))
    dico->instruction = instr_J;
  else if (!strcmp("JR", dico->nom))
    dico->instruction = instr_JR;
  else if (!strcmp("JALR", dico->nom))
    dico->instruction = instr_JALR;
  else if (!strcmp("LB", dico->nom))
    dico->instruction = instr_LB;
  else if (!strcmp("LW", dico->nom))
    dico->instruction = instr_LW;
  else if (!strcmp("LUI", dico->nom))
    dico->instruction = instr_LUI;
  else if (!strcmp("LBU", dico->nom))
    dico->instruction = instr_LBU;
  else if (!strcmp("SB", dico->nom))
    dico->instruction = instr_SB;
  else if (!strcmp("SEB", dico->nom))
    dico->instruction = instr_SEB;
  else if (!strcmp("SW", dico->nom))
    dico->instruction = instr_SW;
  else if (!strcmp("MFLO", dico->nom))
    dico->instruction = instr_MFLO;
  else if (!strcmp("MFHI", dico->nom))
    dico->instruction = instr_MFHI;
  else if (!strcmp("OR", dico->nom))
    dico->instruction = instr_OR;
  else if (!strcmp("ORI", dico->nom))
    dico->instruction = instr_ORI;
  else if (!strcmp("XOR", dico->nom))
    dico->instruction = instr_XOR;
  else if (!strcmp("NOP", dico->nom))
    dico->instruction = instr_NOP;
  else if (!strcmp("SLL", dico->nom))
    dico->instruction = instr_SLL;
  else if (!strcmp("SRL", dico->nom))
    dico->instruction = instr_SRL;
  else if (!strcmp("SRA", dico->nom))
  dico->instruction = instr_SRA;
  else if (!strcmp("SLT", dico->nom))
    dico->instruction = instr_SLT;
  else if (!strcmp("SLTI", dico->nom))
    dico->instruction = instr_SLTI;
  else if (!strcmp("SLTU", dico->nom))
    dico->instruction = instr_SLTU;
  else if (!strcmp("SLTUI", dico->nom))
    dico->instruction = instr_SLTUI;
  else if (!strcmp("SYSCALL", dico->nom))
    dico->instruction = instr_SYSCALL;
  else if (!strcmp("BREAK", dico->nom))
    dico->instruction = instr_BREAK;
  else if (!strcmp("ADD", dico->nom))
    dico->instruction = instr_ADD;
  else if (!strcmp("ADDI", dico->nom))
    dico->instruction = instr_ADDI;
  else if (!strcmp("ADDIU", dico->nom))
    dico->instruction = instr_ADDIU;
  else if (!strcmp("ADDU", dico->nom))
    dico->instruction = instr_ADDU;
  else if (!strcmp("SUB", dico->nom))
    dico->instruction = instr_SUB;
  else if (!strcmp("SUBU", dico->nom))
    dico->instruction = instr_SUBU;
  else if (!strcmp("MULT", dico->nom))
    dico->instruction = instr_MULT;
  else if (!strcmp("DIV", dico->nom))
    dico->instruction = instr_DIV;
  else
    dico->instruction = toutvabien;
}

void dico_del(dico_s d)
{
  int i,j;
  for(i = 0; i < NB_INSTR;i++) // Pour chaque instruction
  {
    if ((d+i)->codeV != NULL) code_del((d+i)->codeV);
    if ((d+i)->nom != NULL) free((d+i)->nom);
    if( (d+i)->op != NULL)
    {
      for(j = 0; j < (d+i)->nbOp;j++)
        if(((d+i)->op)[j] != NULL) free(((d+i)->op)[j]);
      free((d+i)->op);
    }
  }
  free(d);
}

code_s get_dico_code(dico_s d)
{
  return d->codeV;
}

uint32_t get_dico_masque(dico_s d)
{
  return d->masque;
}

char get_dico_type(dico_s d)
{
  return d->type;
}

char* get_dico_nom(dico_s d)
{
  return d->nom;
}

char** get_dico_op(dico_s d)
{
  return d->op;
}

int get_dico_nbOp(dico_s d)
{
  return d->nbOp;
}

instruction_s instru_from_dico(code_s word, dico_s d)
{
  instruction_s instr = calloc(1,sizeof(*instr));
  instr->code = word;
  instr->type = d->type;
  instr->nom = d->nom;
  instr->op = d->op;
  instr->nbOp = d->nbOp;
  instr->affichage = d->affichage;
  instr->instruction = d->instruction;
  return instr;
}
