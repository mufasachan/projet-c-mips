/**
  * @file emulMips_load.c
  * @author Mathias Montginoux
  * @brief commande load
  * @Creation 02/10/2019
  * @Update ...
*/

#include "emulMips_load.h"

#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include "common/bits.h"
#include "common/notify.h"
#include "elf/elf.h"
#include "elf/syms.h"
#include "elf/relocator.h"
#include "mem.h"
#include "mem_elfapi.h"

// On fixe ici une adresse basse dans la mémoire virtuelle. Le premier segment
// ira se loger à cette adresse.
#define START_MEM 0x3000
// nombre max de sections que l'on extraira du fichier ELF
#define NB_SECTIONS 4

// nom de chaque section
#define TEXT_SECTION_STR ".text"
#define RODATA_SECTION_STR ".rodata"
#define DATA_SECTION_STR ".data"
#define BSS_SECTION_STR ".bss"

//nom du prefix à appliquer pour la section
#define RELOC_PREFIX_STR ".rel"


/*--------------------------------------------------------------------------  */
/** Fonction permettant de verifier si une chaine de caracteres
 * est bien dans la liste des symboles du fichier ELF
 * @param name le nom de la chaine recherchée
 * @param symtab la table des symboles
 *
 * @return 1 si present, 0 sinon
 */

int is_in_symbols(char* name, stab symtab) {
    int i;
    for (i=0; i<symtab.size; i++) {
        if (!strcmp(symtab.sym[i].name,name)) return 1;
    }
    return 0;
}

/*--------------------------------------------------------------------------  */
/**
 * Cette fonction calcule le nombre de segments à prevoir
 * Elle cherche dans les symboles si les sections predefinies
 * s'y trouve
 * @param symtab la table des symboles
 *
 * @return le nombre de sections trouvées
 */

unsigned int get_nsegments(stab symtab,char* section_names[],int nb_sections) {
    unsigned int n=0;
    int i;
    for (i=0; i<nb_sections; i++) {
        if (is_in_symbols(section_names[i],symtab)) n++;
    }
    return n;
}


/*--------------------------------------------------------------------------  */
/**
 * fonction permettant d'extraire une section du fichier ELF et de la chargée dans le segment du même nom
 * @param fp  		le pointeur du fichier ELF
 * @param memory      	la structure de mémoire virtuelle
 * @param scn         	le nom de la section à charger
 * @param permission  	l'entier représentant les droits de lecture/ecriture/execution
 * @param add_start   	l'addresse virtuelle à laquelle la section doit être chargée
 *
 * @return  0 en cas de succes, une valeur non nulle sinon
 */
int elf_load_section_in_memory(FILE* fp, mem memory, char* scn,unsigned int permissions,unsigned long long add_start) {
    byte *ehdr    = __elf_get_ehdr( fp );
    byte *content = NULL;
    uint  textsz  = 0;
    vsize sz;
    vaddr addr;

    byte *useless = elf_extract_section_header_table( ehdr, fp );
    free( useless );

    if ( NULL == ehdr ) {
        WARNING_MSG( "Can't read ELF file" );
        return 1;
    }

    if ( 1 == attach_scn_to_mem(memory, scn, SCN_ATTR( WIDTH_FROM_EHDR( ehdr ), permissions ) ) ) {
        WARNING_MSG( "Unable to create %s section", scn );
        free( ehdr );
        return 1;
    }

    content = elf_extract_scn_by_name( ehdr, fp, scn, &textsz, NULL );
    if ( NULL == content ) {
        WARNING_MSG( "Corrupted ELF file" );
        free( ehdr );
        return 1;
    }

    switch( WIDTH_FROM_EHDR(ehdr) ) {
    case 32 :
        sz._32   = textsz/*+8*/; /* +8: In case adding a final sys_exit is needed */
        addr._32 = add_start;
        break;
    case 64 :
        sz._64   = textsz/*+8*/; /* +8: In case adding a final sys_exit is needed */
        addr._64 = add_start;
        break;
    default :
        WARNING_MSG( "Wrong machine width" );
        return 1;
    }

    if ( 1 == fill_mem_scn(memory, scn, sz, addr, content ) ) {
        free( ehdr );
        free( content );
        WARNING_MSG( "Unable to fill in %s segment", scn );
        return 1;
    }

    free( content );
    free( ehdr );

    return 0;
}



/*--------------------------------------------------------------------------  */
/**
 * @param fp le fichier elf original
 * @param seg le segment à reloger
 * @param mem l'ensemble des segments
 *
 * @brief Cette fonction effectue la relocation du segment passé en parametres
 * @brief l'ensemble des segments doit déjà avoir été chargé en memoire.
 *
 * VOUS DEVEZ COMPLETER CETTE FONCTION POUR METTRE EN OEUVRE LA RELOCATION !!
 */
void reloc_segment(FILE* fp, segment seg, mem memory,unsigned int endianness,stab symtab) {
    byte *ehdr    = __elf_get_ehdr( fp );
    uint32_t  scnsz  = 0;
    Elf32_Rel *rel = NULL;
    char* reloc_name = malloc(strlen(seg.name)+strlen(RELOC_PREFIX_STR)+1);
    scntab section_tab;

    // on recompose le nom de la section
    memcpy(reloc_name,RELOC_PREFIX_STR,strlen(RELOC_PREFIX_STR)+1);
    strcat(reloc_name,seg.name);

    // on récupere le tableau de relocation et la table des sections
    rel = (Elf32_Rel *)elf_extract_scn_by_name( ehdr, fp, reloc_name, &scnsz, NULL );
    elf_load_scntab(fp ,32, &section_tab);


		//TODO : faire la relocation ci-dessous !
    if (rel != NULL &&seg.content!=NULL && seg.size._32!=0) {

        INFO_MSG("--------------Relocation de %s-------------------\n",seg.name) ;
        INFO_MSG("Nombre de symboles a reloger: %ld\n",scnsz/sizeof(*rel)) ;

 	//------------------------------------------------------

        //TODO : faire la relocation ICI !

        //------------------------------------------------------


    }
    del_scntab(section_tab);
    free( rel );
    free( reloc_name );
    free( ehdr );

}



// fonction affichant les octets d'un segment sur la sortie standard
// parametres :
//   seg        : le segment de la mémoire virtuelle à afficher

void print_segment_raw_content(segment* seg) {
    int k;
    int word =0;
    if (seg!=NULL && seg->size._32>0) {
        for(k=0; k<seg->size._32; k+=4) {
            if(k%16==0) printf("\n  0x%08x ",k);
            word = *((unsigned int *) (seg->content+k));
            FLIP_ENDIANNESS(word);
            printf("%08x ",	word);
        }
    }
}


/*  Copie le contenu mémoire d'un segment chargé par la lecture du fichier */
/* ElF (structure du projet 'elfapi') dans le segment de la structure mémoire */
/* chargé par 'emulMips' */
void copy_mem(segment_s ram, segment seg){
  if( (ram == NULL) || (ram->data == NULL) || ( seg.content == NULL) ){
    WARNING_MSG("segment_s ram or ram->data or seg.content is NULL in 'copy_mem'");
  }
  else{
    int k = 0;
    for ( k=0; k<seg.size._32 ;k++){
      if( (ram->data+k) == NULL || (seg.content+k) == NULL ){
        WARNING_MSG("ram->data+k or seg.content+k is NULL");
      }
      else{
        *(ram->data+k) = *(seg.content+k);
      }
    }
  }
}


/*  Modifie les segments chargés en mémoire par 'emulMips' avec les données des */
/* segments chargés lors de la lecture du fichier ELF*/
void edit_seg(cpu_s cpu, mem memory){
  DEBUG_MSG("Command: load/fonction edit_seg");
  int i = 0;
  for (i=0;i<memory->nseg;i++){
    segment_s ram = get_cpu_mem(cpu);
    set_seg_name(&ram[i], memory->seg[i].name);
    set_seg_adresse(&ram[i], memory->seg[i].start._32);
    set_seg_taille(&ram[i], memory->seg[i].size._32);
    if (ram[i].data!=NULL) free(ram[i].data);
    alloc_1segment_memoire(&ram[i], memory->seg[i].size._32);
    copy_mem(&ram[i], memory->seg[i]);

    switch( SCN_RIGHTS( memory->seg[i].attr ) ) {
    case R__ :
        set_seg_droit(&ram[i], "r--");
        break;
    case RW_ :
        set_seg_droit(&ram[i], "rw-");
        break;
    case R_X :
        set_seg_droit(&ram[i], "r-x");
        break;
    default :
        printf( "???" );
    }
    /*
    puts("");
    printf("Affichage du segment après edit\n");
    printf("%-12s", get_seg_name(ram+i));
    printf("%-5s", get_seg_droit(ram+i));
    printf("Vaddr: 0x%08x", get_seg_adresse(ram+i));
    printf("  Size: %i bytes\n", get_seg_taille(ram+i));
    puts("");
    */
  }
}

/* Fonction qui retourne 0 si l'adresse est bien un multiple de 0x1000, et qui */
/* retourne 1 si ce n'est pas le cas*/
int check_add(uint32_t segment_start){
  if(segment_start%0x1000){
    WARNING_MSG("The address has to be a multiple of 0x1000");
    return 1;
  }
  return 0;
}

/* Fonction qui retourne 0 si l'adresse de chargement du fichier ELF est  */
/* acceptable, retourne 1 si l'adresse de depart est inferieure à 0x1000 ou */
/* supérieure à l'adresse de départ du segments [stack]*/
int check_start_add(uint32_t segment_start){
  DEBUG_MSG("function 'check_start_add'");
  if(segment_start<0x1000 || segment_start>=0x0f7ff000){
    WARNING_MSG("Cannot load at this address");
    WARNING_MSG("The address has to be higher than 0x1000 and lower than 0x0f7ff000");
    return 1;
  }
  // Pas de probleme pour l'adresse de depart
  return 0;
}

/* Fonction qui retourne 0 si l'adresse de chargement du fichier ELF est  */
/* acceptable, retourne 1 en cas de probleme : si l'adresse et la taille */
/*des segments vont dépasser leur plage autorisée. Exemple : en empietant sur   */
/* les adresses des segments [stack] et [vsyscall] sont fixes */
int check_mem_add(mem memory){
  uint32_t last_seg = 0;
  last_seg = memory->seg[2].start._32 + memory->seg[2].size._32;
  if(last_seg>=0x0f7ff000){
    WARNING_MSG("Cannot load at this address");
    WARNING_MSG("The final segment is out of bound");
    return 1;
  }
  // Pas de probleme pour le chargement du fichier
  return 0;
}

int loadCmd(interpreteur inter, cpu_s cpuMips) {

    DEBUG_MSG("Command: load");
    char* token;
    int no_args = 1;
    int return_value;
    if ((token = get_next_token(inter))!=NULL)
    {
      no_args = 0;
      char * argv[MAX_STR];
      argv[1] = token;

      FILE * pf_elf;
/*
      if ((argc < 2) || (argc > 2)) {
          printf("Usage: %s <fichier elf> \n", argv[0]);
          exit(1);
      }
*/
      if ((pf_elf = fopen(argv[1],"r")) == NULL) {
          WARNING_MSG("cannot open file %s", argv[1]);
          return CMD_INVALID_ARGS_RETURN_VALUE;
      }

      else if (!assert_elf_file(pf_elf)) {
          WARNING_MSG("file %s is not an ELF file", argv[1]);
          fclose(pf_elf);
          return CMD_INVALID_ARGS_RETURN_VALUE;
        }
      else{
        return_value = loadCmd_elf(inter, cpuMips, pf_elf, token, &argv[1]);
        return return_value;
      }

    }

    if (no_args){
      WARNING_MSG("%16s: Please enter an ELF file name after 'load'", "Invalid argument");
      WARNING_MSG("%16s: load <file_name> [<address>]","Syntax");
      return CMD_NO_ARGS_RETURN_VALUE;
    }
    return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    return return_value;
}

int loadCmd_elf(interpreteur inter, cpu_s cpuMips, FILE * pf_elf, char * token, char * argv[]){

      DEBUG_MSG("Command: load <elf_file>");
      int return_value = CMD_INVALID_ARGS_RETURN_VALUE;
      if((token=get_next_token(inter)) == NULL)
      {
        WARNING_MSG("No address specified : file loaded at 0x%x by default", START_MEM);
        WARNING_MSG("%16s: load <file_name> [<address>]","Syntax");
        set_reg_val(get_cpu_reg(cpuMips)+35-1,START_MEM);
        return loadCmd_elf_adr(inter, cpuMips, pf_elf, START_MEM, &argv[1]);

      }
      else {
        if (is_hexa_format(token, 32/4)) {
          uint32_t add;
          sscanf(token, "%x", &add);
          if((token=get_next_token(inter)) != NULL){
            WARNING_MSG("Invalid argument: parameter is not needed after debug");
            WARNING_MSG("%16s: load <file_name> [<address>]","Syntax");
            WARNING_MSG("%16s: an hexa 32 bits value","Address");
            fclose(pf_elf);
            return CMD_INVALID_ARGS_RETURN_VALUE;
          }
          int check_multiple = check_add(add);
          if (check_multiple != 0) return CMD_INVALID_ARGS_RETURN_VALUE;

          unsigned int next_segment_start = add;

          int check = check_start_add(next_segment_start);
          if(check!=0) return CMD_INVALID_ARGS_RETURN_VALUE;

          set_reg_val(get_cpu_reg(cpuMips)+35-1,next_segment_start);
          return loadCmd_elf_adr(inter, cpuMips, pf_elf, next_segment_start, &argv[1]);
        }
        else{
          WARNING_MSG("%16s: load <file_name> [<address>]","Syntax");
          WARNING_MSG("%16s: an hexa 32 bits value","Address");
          fclose(pf_elf);
          return CMD_INVALID_ARGS_RETURN_VALUE;
        }
      }

      return return_value;
}

int loadCmd_elf_adr(interpreteur inter, cpu_s cpuMips, FILE * pf_elf, unsigned int next_segment_start, char * argv[]){
        DEBUG_MSG("Command: load <elf_file> [<address>]");

        char* section_names[NB_SECTIONS]= {TEXT_SECTION_STR,RODATA_SECTION_STR,DATA_SECTION_STR,BSS_SECTION_STR};
        unsigned int segment_permissions[NB_SECTIONS]= {R_X,R__,RW_,RW_};
        unsigned int nsegments;
        int i=0,j=0;
        unsigned int type_machine;
        unsigned int endianness;   //little ou big endian
        unsigned int bus_width;    // 32 bits ou 64bits

        /* Partie à modifier pour charger l'implémentation du premier segment en memoire */
        //unsigned int next_segment_start = START_MEM; // compteur pour designer le début de la prochaine section
        int return_value = CMD_OK_RETURN_VALUE;
        mem memory;  // memoire virtuelle, c'est elle qui contiendra toute les données du programme
        stab symtab= new_stab(0); // table des symboles

        // recuperation des info de l'architecture
        elf_get_arch_info(pf_elf, &type_machine, &endianness, &bus_width);
        // et des symboles
        elf_load_symtab(pf_elf, bus_width, endianness, &symtab);


        nsegments = get_nsegments(symtab,section_names,NB_SECTIONS);

        // allouer la memoire virtuelle
        memory=init_mem(nsegments);

        // Ne pas oublier d'allouer les differentes sections
        j=0;
        /*printf("Load section in memory ; segment raw content");*/
        for (i=0; i<NB_SECTIONS; i++) {
            if (is_in_symbols(section_names[i],symtab)) {
                elf_load_section_in_memory(pf_elf,memory, section_names[i],segment_permissions[i],next_segment_start);
                next_segment_start+= ((memory->seg[j].size._32+0x1000)>>12 )<<12; // on arrondit au 1k suppérieur
                //print_segment_raw_content(&memory->seg[j]); puts("");
                j++;
            }
        }

        // On vérifie que l'adresse de chargement et la taille du dernier
        // segment ne vont pas empieter sur les segments [stack] et [vsyscall]

        int check = check_mem_add(memory);
        if (check != 0){
          // on fait le ménage avant de partir
          del_mem(memory);
          del_stab(symtab);
          fclose(pf_elf);
          return_value = CMD_INVALID_ARGS_RETURN_VALUE;
          return return_value;
        }
        for (i=0; i<nsegments; i++) {
            reloc_segment(pf_elf, memory->seg[i], memory,endianness,symtab);

        }

        //TODO allouer la pile (et donc modifier le nb de segments)

        edit_seg(cpuMips, memory);

        /*printf("\n------ Fichier ELF \"%s\" : sections lues lors du chargement ------\n", argv[1]) ;
        print_mem(memory);
        stab32_print( symtab);*/

        // on fait le ménage avant de partir
        del_mem(memory);
        del_stab(symtab);
        fclose(pf_elf);

        //puts("");
        return return_value;
}
