#include "emulMips_set.h"

int setCmd(interpreteur inter, cpu_s cpuMips)
{
  DEBUG_MSG("Command: set");
  char* token;
  int no_args = 1;
  int return_value;

  if ((token = get_next_token(inter))!=NULL)
  {
    no_args = 0;
    if (strcmp(token,"mem") == 0)
      return_value = setCmdRAM(inter, get_cpu_mem(cpuMips));
    else if(strcmp(token,"reg") == 0)
      return_value = setCmdReg(inter, get_cpu_reg(cpuMips));
    else
    {
      WARNING_MSG("%16s: parameter mem or reg is required after set","Invalid argument");
      WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%16s: set reg <register> <value>","Syntax");
      WARNING_MSG("%16s: an hexa 32 bits value","Address");
      WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
      WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }

  if(no_args) {
      WARNING_MSG("%11s: parameter mem or reg is required after set","No argument");
      WARNING_MSG("%11s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%11s: set reg <register> <value>","Syntax");
      WARNING_MSG("%11s: an hexa 32 bits value","Address");
      WARNING_MSG("%11s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%11s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
      WARNING_MSG("%11s: a \'type\' value (integer or hexa)","Value");
      return CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}


int setCmdRAM(interpreteur inter, RAM_s memmap)
{
  DEBUG_MSG("Command: set mem");
  char* token;
  int no_args = 1;
  int return_value;

  if ((token = get_next_token(inter))!=NULL)
  {
    no_args = 0;
    if (is_hexa_format(token,32/4))
      return_value = setCmdRAMType(inter, memmap,token);
    else
    {
      WARNING_MSG("%16s: parameter address is required after mem","Invalid argument");
      WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%16s: an hexa 32 bits value","Address");
      WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }

  if(no_args) {
      WARNING_MSG("%11s: parameter address is required after mem","No argument");
      WARNING_MSG("%11s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%11s: an hexa 32 bits value","Address");
      WARNING_MSG("%11s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%11s: a \'type\' value (integer or hexa)","Value");
      return CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int setCmdRAMType(interpreteur inter, RAM_s memmap, char* chaine)
{
  DEBUG_MSG("Command: set mem adress");
  char* token;
  int no_args = 1;
  int return_value;

  if ((token = get_next_token(inter))!=NULL)
  {
    no_args = 0;
    if (strcmp(token,"word") == 0)
      return_value = setCmdRAMTypeWord(inter, memmap, chaine);
    else if (strcmp(token, "byte") == 0)
      return_value = setCmdRAMTypeByte(inter, memmap, chaine);
    else
    {
      WARNING_MSG("%16s: parameter type required after address","Invalid argument");
      WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%16s: an hexa 32 bits value","Address");
      WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
      return_value = CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }

  if(no_args) {
      WARNING_MSG("%11s: parameter type is required after address","No argument");
      WARNING_MSG("%11s: set mem <address> <type> <value>","Syntax");
      WARNING_MSG("%11s: an hexa 32 bits value","Address");
      WARNING_MSG("%11s: byte or word (i.e., 8 bits ou 32 bits)","Type");
      WARNING_MSG("%11s: a \'type\' value (integer or hexa)","Value");
      return CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int setCmdRAMTypeByte(interpreteur inter, RAM_s memmap, char* chaine)
{
  DEBUG_MSG("Command: set mem byte");
  char* token;
  char* emptyToken;

  //  On vérifie la présence d'argument
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter value is required after byte","No argument");
    WARNING_MSG("%11s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%11s: an hexa 32 bits value","Address");
    WARNING_MSG("%11s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%11s: a \'type\' value (integer or hexa)","Value");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if((emptyToken = get_next_token(inter)) != NULL)
  {
    WARNING_MSG("%16s: no parameter needed after value","Invalid argument");
    WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  //  On vérifie que l'argument est correct
  if(!is_hexa_format(token, 8/4) && !is_number8(token))
  {
    WARNING_MSG("%16s: parameter value 8 bits required after byte","Invalid argument");
    WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t add;
  uint8_t val;
  sscanf(chaine, "%x", &add);
  sscanf(token, "%hhx", &val);

  //  On localise le segment mémoire
  segment_s p;
  if((p = seg_locate(memmap, add)) ==NULL)
  {
    WARNING_MSG("%16s: address is below the first segment (out of bound)","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  if(!is_writable(p))
  {
    WARNING_MSG("%16s: the memory segment %s is not writable", "Invalid argument", get_seg_name(p));
    WARNING_MSG("%16s: please type disp mem map for more details about segments' authorization","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  int taille = get_seg_taille(p);
  uint32_t firstAdd = get_seg_adresse(p);
  int decalage = add - firstAdd;
  uint8_t* data = get_seg_data(p);

  //  On vérifie que cette adresse est bien définie
  if(decalage > taille)
  {
    WARNING_MSG("%16s: the adress is out of bound","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  On met bien la valeur en place
  DEBUG_MSG("Address 0x%08x is set to 0x%02x", add, val);
  *(data+decalage) = val;

  return CMD_OK_RETURN_VALUE;
}

int setCmdRAMTypeWord(interpreteur inter, RAM_s memmap, char* chaine)
{
  DEBUG_MSG("Command: set mem word");
  char* token;
  char* emptyToken;
  //  On vérifie la présence d'argument
  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter value is required after word","No argument");
    WARNING_MSG("%11s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%11s: an hexa 32 bits value","Address");
    WARNING_MSG("%11s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%11s: a \'type\' value (integer or hexa)","Value");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if((emptyToken = get_next_token(inter)) != NULL)
  {
    WARNING_MSG("%16s: no parameter needed after value","Invalid argument");
    WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  //  On vérifie que l'argument est correct
  if(!is_hexa_format(token, 32/4) && !is_number32(token))
  {
    WARNING_MSG("%16s: parameter value 32 bits required after word","Invalid argument");
    WARNING_MSG("%16s: set mem <address> <type> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: byte or word (i.e., 8 bits ou 32 bits)","Type");
    WARNING_MSG("%16s: a \'type\' value (integer or hexa)","Value");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t add;
  uint32_t val;

  sscanf(chaine, "%x", &add);
  sscanf(token, "%x", &val);

  //  On découpe en 4, comme ça c'est facile pour set après
  uint8_t* valT = calloc(4, sizeof(*valT));
  *valT = val/0x1000000;
  *(valT+1) = (val/0x10000)%0x100;
  *(valT+2) = (val/0x100)%0x100;
  *(valT+3) = val%0x100;
  //  On localise le segment mémoire
  segment_s p;
  if((p = seg_locate(memmap, add))==NULL)
  {
    WARNING_MSG("%16s: address is below the first segment (out of bound)","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  if(!is_writable(p))
  {
    WARNING_MSG("%16s: the memory segment %s is not writable", "Invalid argument", get_seg_name(p));
    WARNING_MSG("%16s: please type disp mem map for more details about segments' authorization","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  int taille = get_seg_taille(p);
  uint32_t firstAdd = get_seg_adresse(p);
  int decalage = add - firstAdd;
  uint8_t* data = get_seg_data(p);

  //  On vérifie que cette adresse est bien définie
  if(decalage > taille - 4)
  {
    WARNING_MSG("%16s: the adress is out of bound","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  //  On met bien la valeur en place
  DEBUG_MSG("Address 0x%08x:0x%08x is set to 0x%08x", add, add+4, val);
  int i;
  for(i=0;i < 4; i++)
  {
    *(data+decalage+i) = *(valT+i);
  }

  return CMD_OK_RETURN_VALUE;
}

int setCmdReg(interpreteur inter, registreTable_s regT)
{
  DEBUG_MSG("Commande set > reg");
  char* token;
  int no_args = 1;
  int return_value;

  if((token = get_next_token(inter)) != NULL)
  {
    no_args = 0;
    if(is_reg(token))
      return_value = setCmdRegVal(inter, regT, token);
    else
    {
      WARNING_MSG("%16s: parameter register is required after reg","Invalid argument");
      WARNING_MSG("%16s: set reg <register> <value>","Syntax");
      WARNING_MSG("%16s: an hexa 32 bits value","Address");
      WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
      return_value =  CMD_INVALID_ARGS_RETURN_VALUE;
    }
  }

  if(no_args)
  {
    WARNING_MSG("%11s: parameter register is required after reg","No argument");
    WARNING_MSG("%11s: set reg <register> <value>","Syntax");
    WARNING_MSG("%11s: an hexa 32 bits value","Address");
    WARNING_MSG("%11s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
    return_value = CMD_NO_ARGS_RETURN_VALUE;
  }

  return return_value;
}

int setCmdRegVal(interpreteur inter, registreTable_s regT, char* chaine)
{
  char* token;
  char* emptyToken;

  if((token = get_next_token(inter)) == NULL)
  {
    WARNING_MSG("%11s: parameter value is required after register","No argument");
    WARNING_MSG("%11s: set reg <register> <value>","Syntax");
    WARNING_MSG("%11s: an hexa 32 bits value","Address");
    WARNING_MSG("%11s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if((emptyToken = get_next_token(inter)) != NULL)
  {
    WARNING_MSG("%16s: no parameter needed after value","Invalid argument");
    WARNING_MSG("%16s: set reg <register> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
    return CMD_NO_ARGS_RETURN_VALUE;
  }

  if(!is_hexa_format(token, 32/4) && !is_number32(token))
  {
    WARNING_MSG("%16s: parameter value 32 bits is required after register","Invalid argument");
    WARNING_MSG("%16s: set reg <register> <value>","Syntax");
    WARNING_MSG("%16s: an hexa 32 bits value","Address");
    WARNING_MSG("%16s: a string included in {$0 ...$31, $zero ...$ra, $hi, $lo, $pc}","Register");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }

  uint32_t valeur;
  sscanf(token, "%x", &valeur);
  registre_s reg =reg_locate(regT,chaine);
  if(!strcmp(get_reg_name(reg),"zero"))
  {
    WARNING_MSG("%16s: register zero is not writable","Invalid argument");
    return CMD_INVALID_ARGS_RETURN_VALUE;
  }
  DEBUG_MSG("Register %s is set to 0x%08x", get_reg_name(reg), valeur);
  set_reg_val(reg, valeur);

  return CMD_OK_RETURN_VALUE;
}
